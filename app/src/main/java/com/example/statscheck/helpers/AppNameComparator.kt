package com.example.statscheck.helpers

import android.app.usage.UsageStats
import java.util.Comparator

class AppNameComparator(private val mAppLabelList: Map<String, String>) : Comparator<UsageStats> {
    override fun compare(a: UsageStats, b: UsageStats): Int {
        val alabel = mAppLabelList[a.packageName]!!
        val blabel = mAppLabelList[b.packageName]!!
        return alabel.compareTo(blabel)
    }
}