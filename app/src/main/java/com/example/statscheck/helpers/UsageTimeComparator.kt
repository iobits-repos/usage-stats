package com.example.statscheck.helpers

import android.app.usage.UsageStats
import java.util.Comparator

class UsageTimeComparator : Comparator<UsageStats> {
    override fun compare(a: UsageStats, b: UsageStats): Int {
        return (b.totalTimeInForeground - a.totalTimeInForeground).toInt()
    }
}