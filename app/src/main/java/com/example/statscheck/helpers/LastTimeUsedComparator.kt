package com.example.statscheck.helpers

import android.app.usage.UsageStats
import java.util.Comparator

class LastTimeUsedComparator : Comparator<UsageStats> {
    override fun compare(a: UsageStats, b: UsageStats): Int {
        // return by descending order
        return (b.lastTimeUsed - a.lastTimeUsed).toInt()
    }
}