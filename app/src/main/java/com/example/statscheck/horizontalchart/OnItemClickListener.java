
package com.example.statscheck.horizontalchart;

public interface OnItemClickListener {
    public void onItemClick(BarItem barItem);
}