package com.iobits.identification.di

import android.app.Application
import android.content.Context
import com.example.statscheck.acitvities.*
import com.example.statscheck.fragments.BoostFragment
import com.example.statscheck.fragments.DashBoardFragment
import com.example.statscheck.fragments.SplashFragment
import com.example.statscheck.fragments.StatsFragment
import com.example.statscheck.fragments.dashboardfragments.DailyDashboardFragment
import com.example.statscheck.fragments.dashboardfragments.WeeklyDashboardFragment
import com.example.statscheck.fragments.intervalfragments.DailyFragment
import com.example.statscheck.fragments.intervalfragments.MonthlyFragment
import com.example.statscheck.fragments.intervalfragments.WeeklyFragment
import com.example.statscheck.fragments.mostusagefragments.ByMonthFragment
import com.example.statscheck.fragments.mostusagefragments.ByTodayFragment
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector
import javax.inject.Singleton

const val TAG = "DaggerModules"

@Module
class AppModule {
    @Provides
    @Singleton
    fun providesContext(application: Application): Context {
        return application
    }

}

@Module
class RetrofitModule {

    /*@Singleton
    @Provides
    fun dailyMotionClient(): OkHttpClient {
        val client = OkHttpClient.Builder()
                .addInterceptor {
                    var request = it.request()
                    val url = request.url().newBuilder()
                            .addQueryParameter("language", DailyMotionApi.LOCLALIZATION)
                            .build()
                    request = request.newBuilder().url(url).build()
                    it.proceed(request)
                }.build()

        return client
    }*/


}


@Module
abstract class UiComponentModule {

    @ContributesAndroidInjector
    abstract fun contributeSplashActivity(): SplashActivity?

    @ContributesAndroidInjector
    abstract fun contrbuteBaseActivity(): BaseActivity?

    @ContributesAndroidInjector
    abstract fun contrbuteMainActivity(): MainActivity?

    @ContributesAndroidInjector
    abstract fun contrbuteUsageStatsActivity(): UsageStatsActivity?

    @ContributesAndroidInjector
    abstract fun contrbuteDetaScreenTimeActivity(): DetailScreenTimeActivity?

    @ContributesAndroidInjector
    abstract fun contrbuteHistoryActivity(): HistoryActivity?

    @ContributesAndroidInjector
    abstract fun contrbuteBoostActivity(): BoostActivity?


    @ContributesAndroidInjector
    abstract fun contributeBoostFragment(): BoostFragment?

    @ContributesAndroidInjector
    abstract fun contributeDashBoardFragment(): DashBoardFragment?

    @ContributesAndroidInjector
    abstract fun contributeSplashFragment(): SplashFragment?

    @ContributesAndroidInjector
    abstract fun contributeStatsFragment(): StatsFragment?

    @ContributesAndroidInjector
    abstract fun contributeDailyFragment(): DailyFragment?

    @ContributesAndroidInjector
    abstract fun contributeMonthlyFragment(): MonthlyFragment?

    @ContributesAndroidInjector
    abstract fun contributeWeeklyFragment(): WeeklyFragment?

    @ContributesAndroidInjector
    abstract fun contributeDailyDashboardFragment(): DailyDashboardFragment?

    @ContributesAndroidInjector
    abstract fun contributeWeeklyDashboardFragment(): WeeklyDashboardFragment?

    @ContributesAndroidInjector
    abstract fun contributeByTodayFragment(): ByTodayFragment?

    @ContributesAndroidInjector
    abstract fun contributeByMonthFragment(): ByMonthFragment?


}
