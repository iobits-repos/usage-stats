package com.iobits.identification.di

import android.app.Application
import com.example.statscheck.ApplicationClass
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton


@Singleton
@Component(modules = [AndroidSupportInjectionModule::class, AppModule::class, UiComponentModule::class])

interface AppComponent : AndroidInjector<ApplicationClass> {

    override fun inject(instance: ApplicationClass)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }
}