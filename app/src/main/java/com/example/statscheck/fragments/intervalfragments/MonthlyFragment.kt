package com.example.statscheck.fragments.intervalfragments

import android.app.usage.UsageStats
import android.app.usage.UsageStatsManager
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.ArrayMap
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.MutableLiveData
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.anychart.AnyChart
import com.anychart.chart.common.dataentry.DataEntry
import com.anychart.chart.common.dataentry.ValueDataEntry
import com.anychart.chart.common.listener.Event
import com.anychart.chart.common.listener.ListenersInterface
import com.anychart.enums.Anchor
import com.anychart.enums.HoverMode
import com.anychart.enums.Position
import com.anychart.enums.TooltipPositionMode
import com.example.statscheck.Constants
import com.example.statscheck.acitvities.DetailScreenTimeActivity
import com.example.statscheck.adapters.StatsAdapter
import com.example.statscheck.databinding.FragmentMonthlyBinding
import com.example.statscheck.fragments.BaseFragment
import com.example.statscheck.helpers.AppNameComparator
import com.example.statscheck.helpers.LastTimeUsedComparator
import com.example.statscheck.helpers.UsageTimeComparator
import com.example.statscheck.managers.AdsManager
import com.iobits.identification.managers.MoPubAdsManager
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject


class MonthlyFragment :  BaseFragment() {
    private val TAG = "MonthlyFragment"
    private var _binding: FragmentMonthlyBinding? = null
    private val binding get() = _binding
    private var mUsageStatsManager: UsageStatsManager? = null
    private lateinit var mInflater: LayoutInflater
    private lateinit var mAdapter: StatsAdapter
    private lateinit var mPm: PackageManager
    @Inject
    lateinit var adsManager: AdsManager
    private val mPackageStats = ArrayList<UsageStats>()
    private val mAppLabelMap = ArrayMap<String, String>()
    private val mUsageTimeComparator = UsageTimeComparator()
    private lateinit var mAppLabelComparator: AppNameComparator
    private val mLastTimeUsedComparator = LastTimeUsedComparator()
    private val readyForset = MutableLiveData<Boolean>()
    private var total_screen_time: Long = 0
    @Inject
    lateinit var moPubAdsManager: MoPubAdsManager
//    var moPubAdsManager: MoPubAdsManager? = null
//    var preferenceManager: PreferenceManager? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        _binding = FragmentMonthlyBinding.inflate(inflater, container, false)


        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //because of animateLayoutChange there is crash of viewGroup . to remove that below line is used.
        binding?.root?.layoutTransition?.setAnimateParentHierarchy(false)
        loadAd()
        init()
        fetchData()
        getWeekDays()
    }
    private fun sendDrawerUpdateBroadCast() {
        // Create intent with action
        Log.e("isUserSignedIn","sendDrawerUpdateBroadCast in create user" )
        val localIntent = Intent(Constants.DRAWER_UPDATE_EVENT)
        localIntent.putExtra(Constants.SCREEENTIME_MONTHLY,true)
        localIntent.putExtra(Constants.SCREEENTIME_DRAWER,total_screen_time)
        LocalBroadcastManager.getInstance(requireContext()).sendBroadcast(localIntent)
    }
    private fun loadAd() {
//        moPubAdsManager.onSDKintialized(object : MoPubAdsManager.ISDKinitialize {
//            override fun onIntialized(isInit: Boolean) {
//                binding?.adFrame?.removeAllViews()
//                binding?.adFrame?.let { moPubAdsManager.loadNativeAds(it, Constants.MOPUB_NATIVE, MoPubAdsManager.NativeAdType.SMALL_TYPE) }
//            }
//        })
//
//        if (moPubAdsManager.isInitialized) {
//            binding?.adFrame?.removeAllViews()
//            binding?.adFrame?.let { moPubAdsManager.loadNativeAds(it, Constants.MOPUB_NATIVE, MoPubAdsManager.NativeAdType.SMALL_TYPE) }
//        }
        adsManager.loadNativeAd(requireActivity(),  binding?.adFrame, AdsManager.NativeAdType.NOMEDIA_MEDIUM)
    }

    private fun init() {
        mUsageStatsManager = requireContext().getSystemService(AppCompatActivity.USAGE_STATS_SERVICE) as UsageStatsManager
        mInflater = requireContext().getSystemService(AppCompatActivity.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        mPm = requireContext().packageManager
        // only create and set a new adapter if there isn't already one
        mAdapter = StatsAdapter()
        val moPubAdapter = moPubAdsManager.getMoPubAdapter(requireActivity(), mAdapter, MoPubAdsManager.NativeAdType.BANNER_ADAPTER_TYPE)
        binding?.statsRecyclerView.apply {
            this?.layoutManager = LinearLayoutManager(context)
            this?.adapter = moPubAdapter
        }
        moPubAdapter.loadAds(Constants.MOPUB_NATIVE)
//        binding?.statsRecyclerView?.setLayoutManager(LinearLayoutManager(requireContext()))
//        binding?.statsRecyclerView?.setAdapter(mAdapter)

        mAdapter.onClick = { position ->
            val label = mAppLabelMap.get(mPackageStats.get(position).packageName)
            val intent = Intent(activity, DetailScreenTimeActivity::class.java)
            intent.putParcelableArrayListExtra("StatUsageList",mPackageStats)
            intent.putExtra("position",position)
            intent.putExtra("AppLabel",label)
            val percentage = (findPercentageInHour(mPackageStats[position].totalTimeInForeground).toDouble() / findPercentageInHour(total_screen_time)) * 100
            intent.putExtra("Percentage", percentage.toString())
            startActivity(intent)
        }
    }
    fun findPercentageInHour(totalTimeInForeground: Long): Int {
        val timeSec: Long = totalTimeInForeground / 1000
        var requiredFormat = 0
        val hours = timeSec.toInt() / 3600
        requiredFormat = hours //hh:mm:ss formatted string
        val temp = timeSec.toInt() - hours * 3600
        val mins = temp / 60
        if (hours==0 && mins >0){
            requiredFormat = 1
            return requiredFormat
        }
        return requiredFormat
    }
    fun fetchData() {
        //run on coroutine thread so everything done in line and app dosnt lag
        CoroutineScope(Dispatchers.IO).launch {
            getalldata()

            withContext(Dispatchers.Main) {
                setData()
//                readyForset.value = true// we cannot setvalue in IO thread thats why Main thread used
            }
        }
    }
    private fun setData() {
        mAdapter.setData(mAppLabelMap, mPackageStats,
                mPm, mInflater, mAppLabelComparator,
                mLastTimeUsedComparator, mUsageTimeComparator)

        binding?.recyclerViewLayout?.visibility = View.VISIBLE
        binding?.adFramCard?.visibility = View.VISIBLE
        binding?.progressCircular?.visibility = View.GONE
        mAdapter.sortList()
        //set graph top date and bottom day
        binding?.screenTimeTxt?.text = timeFormat(total_screen_time)
        sendDrawerUpdateBroadCast()
        binding?.curentDayDate?.text = giveCurrentDate()
//        binding?.curentDayDate?.visibility = View.VISIBLE
        binding?.screenTimeLinearLayout?.visibility = View.VISIBLE
        readyForset.value = false
    }
    fun giveCurrentDate(): String? {
        val cal = Calendar.getInstance()
        val sdf = SimpleDateFormat("EEE, d MMM, yyyy")
        return sdf.format(cal.time)
    }


    private fun setTotalScreenTimeGraph(day1: String, day2: String, day3: String,
                                        day4: String, day5: String, day6: String, day7: String, list: ArrayList<Int>) {
        binding?.monthlyBarChart?.setProgressBar(binding?.graphProgress)
        val cartesian = AnyChart.column()
        cartesian.yAxis(0).title("Hours")
        val data: MutableList<DataEntry> = ArrayList()
        data.add(ValueDataEntry(day1, list.get(0)))
        data.add(ValueDataEntry(day2, list.get(1)))
        data.add(ValueDataEntry(day3, list.get(2)))
        data.add(ValueDataEntry(day4, list.get(3)))
        data.add(ValueDataEntry(day5, list.get(4)))
        data.add(ValueDataEntry(day6, list.get(5)))
        data.add(ValueDataEntry(day7, list.get(6)))

        val column = cartesian.column(data)
        column.tooltip()
                .titleFormat("Hours")
                .position(Position.CENTER_BOTTOM)
                .anchor(Anchor.CENTER_BOTTOM)
                .offsetX(0.0)
                .offsetY(5.0)
                .format("{%Value}")
//                .format("\${%Value}{groupsSeparator: }")

        column.setOnClickListener(object : ListenersInterface.OnClickListener() {
            override fun onClick(event: Event?) {
                Log.d("column", "onClick: ${event.toString()}")
            }

        })
        column.select(6)
        column.fill("#91cbf9")
        column.selected().fill("#2497f3")
        column.normal().stroke("#91cbf9")
        column.selected().stroke("#2497f3")

        cartesian.animation(true)
        cartesian.yScale().minimum(0.0)
        cartesian.tooltip().positionMode(TooltipPositionMode.POINT)
        cartesian.interactivity().hoverMode(HoverMode.BY_X)

        binding?.monthlyBarChart?.setChart(cartesian)
    }

    fun setTimeInFormat(totalTimeInForeground: Long): Int {
        val timeSec: Long = totalTimeInForeground / 1000
        var requiredFormat = 0
        val hours = timeSec.toInt() / 3600
        requiredFormat = hours //hh:mm:ss formatted string
        return requiredFormat
    }

    fun timeFormat(totalTimeInForeground: Long): String {
        val timeSec: Long = totalTimeInForeground / 1000
        var requiredFormat = "0 sec"
        val hours = timeSec.toInt() / 3600
        var temp = timeSec.toInt() - hours * 3600
        val mins = temp / 60
        temp = temp - mins * 60
        val secs = temp
        if (mins > 0) {
            if (hours > 0) {
                requiredFormat = "$hours hr, $mins min" //hh:mm:ss formatted string
            }
            else {
                requiredFormat = "$mins min" //hh:mm:ss formatted string
            }
        }else if(hours > 0){
            requiredFormat = "$hours hr" //hh:mm:ss formatted string
        }else{
            requiredFormat = "$secs secs"
        }
        return requiredFormat
//        Log.d(TAG, "time: $requiredFormat")
    }

    fun getWeekDays() {
        val weekDayOne = Calendar.getInstance()
        val weekDayTwo = Calendar.getInstance()
        val weekDayThree = Calendar.getInstance()
        val weekDayFour = Calendar.getInstance()
        val weekDayFive = Calendar.getInstance()
        val weekDaySix = Calendar.getInstance()
        val weekDaySav = Calendar.getInstance()
        weekDayOne.add(Calendar.DAY_OF_YEAR, -7)//-5 by default
        weekDayTwo.add(Calendar.DAY_OF_YEAR, -6)//-5 by default
        weekDayThree.add(Calendar.DAY_OF_YEAR, -5)//-5 by default
        weekDayFour.add(Calendar.DAY_OF_YEAR, -4)//-5 by default
        weekDayFive.add(Calendar.DAY_OF_YEAR, -3)//-5 by default
        weekDaySix.add(Calendar.DAY_OF_YEAR, -2)//-5 by default
        weekDaySav.add(Calendar.DAY_OF_YEAR, -1)//-5 by default
        val sdf = SimpleDateFormat("EEE")


        val day1ScreenTime = getScreenTodayScreenTime(weekDayOne.timeInMillis, weekDayTwo.timeInMillis)
        val day2ScreenTime = getScreenTodayScreenTime(weekDayTwo.timeInMillis, weekDayThree.timeInMillis)
        val day3ScreenTime = getScreenTodayScreenTime(weekDayThree.timeInMillis, weekDayFour.timeInMillis)
        val day4ScreenTime = getScreenTodayScreenTime(weekDayFour.timeInMillis, weekDayFive.timeInMillis)
        val day5ScreenTime = getScreenTodayScreenTime(weekDayFive.timeInMillis, weekDaySix.timeInMillis)
        val day6ScreenTime = getScreenTodayScreenTime(weekDaySix.timeInMillis, weekDaySav.timeInMillis)
        val day7ScreenTime = getScreenTodayScreenTime(weekDaySav.timeInMillis, System.currentTimeMillis())

        val list: ArrayList<Int> = ArrayList<Int>()
        list.clear()
        list.add(setTimeInFormat(day1ScreenTime.toLong()))
        list.add(setTimeInFormat(day2ScreenTime.toLong()))
        list.add(setTimeInFormat(day3ScreenTime.toLong()))
        list.add(setTimeInFormat(day4ScreenTime.toLong()))
        list.add(setTimeInFormat(day5ScreenTime.toLong()))
        list.add(setTimeInFormat(day6ScreenTime.toLong()))
        list.add(setTimeInFormat(day7ScreenTime.toLong()))

//            withContext(Dispatchers.Main) {
        setTotalScreenTimeGraph(sdf.format(weekDayOne.time), sdf.format(weekDayTwo.time),
                sdf.format(weekDayThree.time),
                sdf.format(weekDayFour.time),
                sdf.format(weekDayFive.time),
                sdf.format(weekDaySix.time),
                sdf.format(weekDaySav.time),
                list
        )

    }

    fun getScreenTodayScreenTime(beginTime: Long, timeInMillis: Long): Int {
        var tempTotalScreenTime = 0

        val stats = mUsageStatsManager?.queryUsageStats(UsageStatsManager.INTERVAL_DAILY,
                beginTime, timeInMillis)
//        val stats= mUsageStatsManager?.queryAndAggregateUsageStats(beginTime, System.currentTimeMillis())

        val map = ArrayMap<String, UsageStats>()
        val statCount = stats!!.size

        for (i in 0 until statCount) {
            val pkgStats = stats[i]
            // load application labels for each application
            try {

                val existingStats = map[pkgStats!!.packageName]
                if (existingStats == null) {
                    val appUseTime = pkgStats.totalTimeInForeground / 1000
                    val neverUsedTime = 0

                    if (appUseTime > neverUsedTime) {
                        //adding pakage into map
                        tempTotalScreenTime = tempTotalScreenTime.plus(pkgStats.totalTimeInForeground).toInt()

                    }

                }
            } catch (e: PackageManager.NameNotFoundException) {
                // This package may be gone.
            }
        }
        return tempTotalScreenTime
    }

    fun getalldata() {
        val cal = Calendar.getInstance()
        cal.add(Calendar.DAY_OF_YEAR, -1)//-5 by default
        val stats = mUsageStatsManager?.queryUsageStats(UsageStatsManager.INTERVAL_MONTHLY,
                cal.timeInMillis, System.currentTimeMillis())
//        mUsageStatsManager?.queryAndAggregateUsageStats()

        val map = ArrayMap<String, UsageStats>()
        val statCount = stats!!.size

        for (i in 0 until statCount) {
            val pkgStats = stats[i]

            // load application labels for each application
            try {
                val appInfo = mPm.getApplicationInfo(pkgStats.packageName, 0)
                val label = mPm.let { appInfo.loadLabel(it).toString() }
                if (!mAppLabelMap.contains(pkgStats.packageName)) {
                    mAppLabelMap[pkgStats.packageName] = label
                } else {
                    break
                }

                val existingStats = map[pkgStats.packageName]
                if (existingStats == null) {
                    val appUseTime = pkgStats.totalTimeInForeground / 1000
                    val neverUsedTime = 0

                    if (appUseTime > neverUsedTime) {
                        //adding pakage into map
                        total_screen_time = total_screen_time.plus(pkgStats.totalTimeInForeground)
//                        total_screen_time= total_screen_time.plus(pkgStats.totalTimeInForeground / 1000)
                        map[pkgStats.packageName] = pkgStats
                    } else {
                        Log.d(TAG, "getalldata: app never used")
                    }

                } else {
                    existingStats.add(pkgStats)
                }
            } catch (e: PackageManager.NameNotFoundException) {
                // This package may be gone.
            }
        }
//        mPackageStats.clear()
        mPackageStats.addAll(map.values)
        // Sort list
        mAppLabelComparator = AppNameComparator(mAppLabelMap)

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}