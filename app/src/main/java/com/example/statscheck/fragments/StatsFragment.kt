package com.example.statscheck.fragments

import android.os.Bundle
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import com.example.statscheck.R
import com.example.statscheck.adapters.IntervalAdapter
import com.example.statscheck.databinding.FragmentStatsBinding
import com.example.statscheck.fragments.intervalfragments.DailyFragment
import com.example.statscheck.fragments.intervalfragments.MonthlyFragment
import com.example.statscheck.fragments.intervalfragments.WeeklyFragment

import java.util.*


class StatsFragment :  BaseFragment() {
    var mLastClickTime=0L
    private var _binding: FragmentStatsBinding? = null
    private val binding get() = _binding
    private lateinit var adapter: IntervalAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        _binding = FragmentStatsBinding.inflate(inflater, container, false)

//        adapter = IntervalAdapter(requireActivity())
//        binding?.intervalViewPager?.offscreenPageLimit = 5
//        binding?.intervalViewPager?.isUserInputEnabled = false
//        binding?.intervalViewPager?.adapter = this.adapter
//        binding?.intervalViewPager?.setCurrentItem(1)



        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //because of animateLayoutChange there is crash of viewGroup . to remove that below line is used.
        binding?.root?.layoutTransition?.setAnimateParentHierarchy(false)
        loadFragment(DailyFragment())
        init()
    }
    private fun init() {
        binding?.dailyIntervl?.setOnClickListener {
            // check if user click again and again before data loading
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1500){
                showSnackBar()
                return@setOnClickListener
            }
            mLastClickTime = SystemClock.elapsedRealtime()
            // user do there other click after 1.5 sec
            binding?.dailyIntervl?.background = ResourcesCompat.getDrawable(getResources(), R.drawable.curved_bg_selected, null)
            binding?.monthlyIntervl?.background = ResourcesCompat.getDrawable(getResources(), R.drawable.curved_bg_unselected, null)
            binding?.weeklyIntervl?.background = ResourcesCompat.getDrawable(getResources(), R.drawable.curved_bg_unselected, null)

            binding?.dailyIntervlText?.setTextColor(ResourcesCompat.getColor(getResources(), R.color.colorWhite, null))
            binding?.monthlyIntervlText?.setTextColor(ResourcesCompat.getColor(getResources(), R.color.text_color_light, null))
            binding?.weeklyIntervlText?.setTextColor(ResourcesCompat.getColor(getResources(), R.color.text_color_light, null))
            loadFragment(DailyFragment())
//            binding!!.intervalViewPager.setCurrentItem(1)
        }
        binding?.weeklyIntervl?.setOnClickListener {
            // check if user click again and again before data loading
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1500){
                showSnackBar()
                return@setOnClickListener
            }
            mLastClickTime = SystemClock.elapsedRealtime()
            // user do there other click after 1.5 sec
            binding?.dailyIntervl?.background = ResourcesCompat.getDrawable(getResources(), R.drawable.curved_bg_unselected, null)
            binding?.monthlyIntervl?.background = ResourcesCompat.getDrawable(getResources(), R.drawable.curved_bg_unselected, null)
            binding?.weeklyIntervl?.background = ResourcesCompat.getDrawable(getResources(), R.drawable.curved_bg_selected, null)
            //
            binding?.dailyIntervlText?.setTextColor(ResourcesCompat.getColor(getResources(), R.color.text_color_light, null))
            binding?.monthlyIntervlText?.setTextColor(ResourcesCompat.getColor(getResources(), R.color.text_color_light, null))
            binding?.weeklyIntervlText?.setTextColor(ResourcesCompat.getColor(getResources(), R.color.colorWhite, null))
            loadFragment(WeeklyFragment())
//            binding!!.intervalViewPager.setCurrentItem(0)
        }
        binding?.monthlyIntervl?.setOnClickListener {
            // check if user click again and again before data loading
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1500){
                showSnackBar()
                return@setOnClickListener
            }
            mLastClickTime = SystemClock.elapsedRealtime()
            // user do there other click after 1.5 sec
            binding?.dailyIntervl?.background = ResourcesCompat.getDrawable(getResources(), R.drawable.curved_bg_unselected, null)
            binding?.monthlyIntervl?.background = ResourcesCompat.getDrawable(getResources(), R.drawable.curved_bg_selected, null)
            binding?.weeklyIntervl?.background = ResourcesCompat.getDrawable(getResources(), R.drawable.curved_bg_unselected, null)
            //
            binding?.dailyIntervlText?.setTextColor(ResourcesCompat.getColor(getResources(), R.color.text_color_light, null))
            binding?.monthlyIntervlText?.setTextColor(ResourcesCompat.getColor(getResources(), R.color.colorWhite, null))
            binding?.weeklyIntervlText?.setTextColor(ResourcesCompat.getColor(getResources(), R.color.text_color_light, null))
            loadFragment(MonthlyFragment())
//            binding!!.intervalViewPager.setCurrentItem(2)
        }


    }

    private fun loadFragment(fragment: Fragment) {
        val ft = childFragmentManager.beginTransaction()
        ft.replace(R.id.root_container, fragment)
        ft.addToBackStack(null)
        ft.commit()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}