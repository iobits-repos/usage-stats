package com.example.statscheck.fragments


import android.os.Bundle
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import com.example.statscheck.R
import com.example.statscheck.adapters.DashBoardAdapter
import com.example.statscheck.databinding.FragmentDashBoardBinding
import com.example.statscheck.fragments.dashboardfragments.DailyDashboardFragment
import com.example.statscheck.fragments.dashboardfragments.WeeklyDashboardFragment
import java.util.*
import kotlin.collections.ArrayList

class DashBoardFragment : BaseFragment() {
    private var _binding: FragmentDashBoardBinding? = null
    private val binding get() = _binding
    var mLastClickTime=0L
    var arrayListHours = ArrayList<Int>()
    private lateinit var adapter: DashBoardAdapter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        _binding = FragmentDashBoardBinding.inflate(inflater, container, false)


        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //because of animateLayoutChange there is crash of viewGroup . to remove that below line is used.
        binding?.root?.layoutTransition?.setAnimateParentHierarchy(false)

        arrayListHours = getHoursUsageMap()
        loadFragment(WeeklyDashboardFragment())
        init()

    }
    private fun loadFragment(fragment: Fragment) {
        val ft = childFragmentManager.beginTransaction()
        ft.replace(R.id.root_container, fragment)
        ft.addToBackStack(null)
        ft.commit()
    }

    private fun init() {
//                adapter = DashBoardAdapter(requireActivity())
//        binding?.viewpager?.offscreenPageLimit = 5
//        binding?.viewpager?.isUserInputEnabled = false
//        binding?.viewpager?.adapter = this.adapter
//        binding?.viewpager?.setCurrentItem(1)

        binding?.dailyIntervl?.setOnClickListener {
            // check if user click again and again before data loading
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1500){
                showSnackBar()
                return@setOnClickListener
            }
            mLastClickTime = SystemClock.elapsedRealtime()
            // user do there other click after 1.5 sec
            binding?.dailyIntervl?.background = ResourcesCompat.getDrawable(getResources(), R.drawable.curved_bg_selected, null)
            binding?.weeklyIntervl?.background = ResourcesCompat.getDrawable(getResources(), R.drawable.curved_bg_unselected, null)

            binding?.dailyIntervlText?.setTextColor(ResourcesCompat.getColor(getResources(), R.color.colorWhite, null))
            binding?.weeklyIntervlText?.setTextColor(ResourcesCompat.getColor(getResources(), R.color.text_color_light, null))
            loadFragment(DailyDashboardFragment())
//            binding?.viewpager?.setCurrentItem(0)

        }
        binding?.weeklyIntervl?.setOnClickListener {
            // check if user click again and again before data loading
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1500){
                showSnackBar()
                return@setOnClickListener
            }
            mLastClickTime = SystemClock.elapsedRealtime()
            // user do there other click after 1.5 sec
            binding?.dailyIntervl?.background = ResourcesCompat.getDrawable(getResources(), R.drawable.curved_bg_unselected, null)
            binding?.weeklyIntervl?.background = ResourcesCompat.getDrawable(getResources(), R.drawable.curved_bg_selected, null)
            //
            binding?.dailyIntervlText?.setTextColor(ResourcesCompat.getColor(getResources(), R.color.text_color_light, null))
            binding?.weeklyIntervlText?.setTextColor(ResourcesCompat.getColor(getResources(), R.color.colorWhite, null))
            loadFragment(WeeklyDashboardFragment())
//            binding?.viewpager?.setCurrentItem(1)
        }
    }
    fun getHoursUsageMap() : ArrayList<Int>{
        val arrayList = ArrayList<Int>()
        val random = Random()
        var randomValue: Int = random.nextInt(6 - 3) + 3
        arrayList.add(randomValue)
        arrayList.add(randomValue)
        randomValue = random.nextInt(2 - 0) + 0
        arrayList.add(randomValue)
        arrayList.add(0)
        arrayList.add(randomValue)
        arrayList.add(2)
        randomValue = random.nextInt(6 - 2) + 2
        arrayList.add(randomValue)
        arrayList.add(randomValue)
        randomValue = random.nextInt(5 - 1) + 1
        arrayList.add(randomValue)
        arrayList.add(randomValue)
        randomValue = random.nextInt(6 - 4) + 4
        arrayList.add(randomValue)
        randomValue = random.nextInt(6 - 4) + 4
        arrayList.add(randomValue)
        return arrayList
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}