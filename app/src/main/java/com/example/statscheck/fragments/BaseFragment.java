package com.example.statscheck.fragments;


import android.widget.Toast;

import androidx.core.content.res.ResourcesCompat;

import com.example.statscheck.R;
import com.example.statscheck.managers.PreferenceManager;
import com.google.android.material.snackbar.Snackbar;

import javax.inject.Inject;
import dagger.android.support.DaggerFragment;

import static com.example.statscheck.managers.PreferenceManager.Key.IS_WELCOME_MESSAGE_ACCEPTED;


public abstract class BaseFragment extends DaggerFragment {
    @Inject
    PreferenceManager preferenceManager;


    public boolean isWelcomeMsgAccepted(){
        return preferenceManager.getBoolean(IS_WELCOME_MESSAGE_ACCEPTED,false);
    }
    public void showSnackBar(){
        Snackbar snackbar = Snackbar.make(getActivity().findViewById(android.R.id.content), "Please wait, Data is Loading!!", Snackbar.LENGTH_SHORT);
        snackbar.setBackgroundTint(ResourcesCompat.getColor(getResources(), R.color.theme_color_blue, null));
        snackbar.show();
    }



}
