package com.example.statscheck.fragments.mostusagefragments

import android.app.usage.UsageStats
import android.app.usage.UsageStatsManager
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.ArrayMap
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.statscheck.R
import com.example.statscheck.adapters.MostUsageAdapter
import com.example.statscheck.databinding.FragmentBoostBinding
import com.example.statscheck.databinding.FragmentByTodayBinding
import com.example.statscheck.fragments.BaseFragment
import com.example.statscheck.helpers.AppNameComparator
import com.example.statscheck.helpers.UsageTimeComparator
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*

class ByTodayFragment : BaseFragment() {
    private var _binding: FragmentByTodayBinding? = null
    private val binding get() = _binding
    private lateinit var mAdapter: MostUsageAdapter
    private var mUsageStatsManager: UsageStatsManager? = null
    private lateinit var mInflater: LayoutInflater
    private lateinit var mPm: PackageManager
    private val mPackageStats = ArrayList<UsageStats>()
    private val mMaxUsageList = ArrayList<UsageStats>()
    private val mAppLabelMap = ArrayMap<String, String>()
    private val mUsageTimeComparator = UsageTimeComparator()
    private lateinit var mAppLabelComparator: AppNameComparator
    private val TAG = "BoostFragment"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        _binding = FragmentByTodayBinding.inflate(inflater, container, false)

        init()

        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //because of animateLayoutChange there is crash of viewGroup . to remove that below line is used.
        binding?.root?.layoutTransition?.setAnimateParentHierarchy(false)
        fetchData()
    }
    fun fetchData() {
        //run on coroutine thread so everything done in line and app dosnt lag
        CoroutineScope(Dispatchers.IO).launch {
            getalldata()
            withContext(Dispatchers.Main) {
                Collections.sort(mPackageStats, mUsageTimeComparator)
                if (mPackageStats.size>5){
                for (i in 0 until 5){
                    mMaxUsageList.add(mPackageStats[i])
                }
                }else{
                    for (i in 0 until mPackageStats.size){
                        mMaxUsageList.add(mPackageStats[i])
                    }
                }
                setData()
            }
        }
    }
    private fun setData() {
        mAdapter.setData(mAppLabelMap, mMaxUsageList,
                mPm)
        binding?.progressCircular?.visibility = View.GONE
    }
    private fun init() {
        mUsageStatsManager = requireContext().getSystemService(AppCompatActivity.USAGE_STATS_SERVICE) as UsageStatsManager
        mInflater = requireContext().getSystemService(AppCompatActivity.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        mPm = requireContext().packageManager

          binding?.mostUsageRecyclerView?.setLayoutManager(LinearLayoutManager(requireContext()))
        // only create and set a new adapter if there isn't already one
        mAdapter = MostUsageAdapter()
        binding?.mostUsageRecyclerView?.setAdapter(mAdapter)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
    fun getalldata() {
        val cal = Calendar.getInstance()
        cal.add(Calendar.DAY_OF_YEAR, -1)//-5 by default
        val stats = mUsageStatsManager?.queryUsageStats(UsageStatsManager.INTERVAL_DAILY,
                cal.timeInMillis, System.currentTimeMillis())
//        mUsageStatsManager?.queryAndAggregateUsageStats()

        val map = ArrayMap<String, UsageStats>()
        val statCount = stats!!.size

        for (i in 0 until statCount) {
            val pkgStats = stats[i]

            // load application labels for each application
            try {
                val appInfo = mPm.getApplicationInfo(pkgStats.packageName, 0)
                val label = appInfo.loadLabel(mPm).toString()
                if (!mAppLabelMap.contains(pkgStats.packageName)) {
                    mAppLabelMap[pkgStats.packageName] = label
                } else {
                    break
                }
                val existingStats = map[pkgStats.packageName]
                if (existingStats == null) {
                    val appUseTime = pkgStats.totalTimeInForeground / 1000
                    val neverUsedTime = 0
                    if (appUseTime > neverUsedTime) { //condition for add only those who used only
                        //adding pakage into map
                        map[pkgStats.packageName] = pkgStats
                    }
                } else {
                    existingStats.add(pkgStats)
                }
            } catch (e: PackageManager.NameNotFoundException) {
                // This package may be gone.
            }
        }
//        mPackageStats.clear()
        mPackageStats.addAll(map.values)
        // Sort list
        mAppLabelComparator = AppNameComparator(mAppLabelMap)

    }
}