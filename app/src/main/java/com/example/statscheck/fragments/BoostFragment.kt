package com.example.statscheck.fragments

import android.app.usage.UsageStats
import android.app.usage.UsageStatsManager
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.SystemClock
import android.text.format.DateUtils
import android.util.ArrayMap
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.palette.graphics.Palette
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.statscheck.Constants
import com.example.statscheck.R
import com.example.statscheck.acitvities.BoostActivity
import com.example.statscheck.acitvities.DetailScreenTimeActivity
import com.example.statscheck.adapters.DetailsAdapter
import com.example.statscheck.adapters.MostUsageAdapter
import com.example.statscheck.adapters.StatsAdapter
import com.example.statscheck.databinding.FragmentBoostBinding
import com.example.statscheck.databinding.FragmentDashBoardBinding
import com.example.statscheck.fragments.dashboardfragments.DailyDashboardFragment
import com.example.statscheck.fragments.dashboardfragments.WeeklyDashboardFragment
import com.example.statscheck.fragments.mostusagefragments.ByMonthFragment
import com.example.statscheck.fragments.mostusagefragments.ByTodayFragment
import com.example.statscheck.helpers.AppNameComparator
import com.example.statscheck.helpers.LastTimeUsedComparator
import com.example.statscheck.helpers.UsageTimeComparator
import com.example.statscheck.managers.AdsManager
import com.ghanshyam.graphlibs.BuildConfig.version
import com.ghanshyam.graphlibs.Graph
import com.ghanshyam.graphlibs.GraphData
import com.iobits.identification.managers.MoPubAdsManager
import com.ram.speed.booster.RAMBooster
import com.ram.speed.booster.interfaces.ScanListener
import com.ram.speed.booster.utils.ProcessInfo
import kotlinx.coroutines.*
import java.util.*
import javax.inject.Inject


class BoostFragment : BaseFragment() {

    private var _binding: FragmentBoostBinding? = null
    private val binding get() = _binding
    private lateinit var mAdapter: MostUsageAdapter
    private val TAG = "BoostFragment"
    var mLastClickTime = 0L
    val tab_orage_color = context?.let { ContextCompat.getColor(it, R.color.tab_color_orange) }

    @Inject
    lateinit var moPubAdsManager: MoPubAdsManager
    @Inject
    lateinit var adsManager: AdsManager
    private val readyForset = MutableLiveData<Boolean>()
    private var mAvailableRam = 0L
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        _binding = FragmentBoostBinding.inflate(inflater, container, false)


        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //because of animateLayoutChange there is crash of viewGroup . to remove that below line is used.
        binding?.root?.layoutTransition?.setAnimateParentHierarchy(false)
        //for delay load this fragment when app is open
        Handler(Looper.getMainLooper()).postDelayed(object : Runnable {
            override fun run() {
                init()
                loadFragment(ByTodayFragment())
                loadAd()
                binding?.adFramCard?.visibility = View.VISIBLE
                initPieChart()
            }
        }, 3000)

    }


    fun init() {

        val booster = RAMBooster(context)
        booster.setScanListener(object : ScanListener {
            override fun onStarted() {
            }

            override fun onFinished(availableRam: Long, totalRam: Long, appsToClean: List<ProcessInfo>) {
                CoroutineScope(Dispatchers.Main).launch {
                    mAvailableRam = availableRam
                    readyForset.value = true
                }
            }
        })
        booster.startScan(true)

        readyForset.observe(requireActivity(), Observer {
            if (it) {
                CoroutineScope(Dispatchers.Main).launch {
                    binding?.availabeRam?.text = mAvailableRam.toString() + " Mb"
                }
            }
        })
        binding?.boostBtn?.setOnClickListener {
            val intent = Intent(activity, BoostActivity::class.java)
            startActivity(intent)
//            CoroutineScope(Dispatchers.Main).launch {
//                delay(1000)
//                binding?.availabeRam?.text = (mAvailableRam + 150).toString() + " Mb"
//            }
        }

        binding?.byToday?.setOnClickListener {
            // check if user click again and again before data loading
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1500) {
                showSnackBar()
                return@setOnClickListener
            }
            mLastClickTime = SystemClock.elapsedRealtime()
            // user do there other click after 1.5 sec
            binding?.byToday?.background = ResourcesCompat.getDrawable(getResources(), R.drawable.curved_bg_selected, null)
            binding?.byMonthlyIntervl?.background = ResourcesCompat.getDrawable(getResources(), R.drawable.curved_bg_unselected, null)

            binding?.byTodayText?.setTextColor(ResourcesCompat.getColor(getResources(), R.color.colorWhite, null))
            binding?.byMonthlyIntervlText?.setTextColor(ResourcesCompat.getColor(getResources(), R.color.text_color_light, null))
            loadFragment(ByTodayFragment())


        }
        binding?.byMonthlyIntervl?.setOnClickListener {
            // check if user click again and again before data loading
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1500) {
                showSnackBar()
                return@setOnClickListener
            }
            mLastClickTime = SystemClock.elapsedRealtime()
            // user do there other click after 1.5 sec
            binding?.byToday?.background = ResourcesCompat.getDrawable(getResources(), R.drawable.curved_bg_unselected, null)
            binding?.byMonthlyIntervl?.background = ResourcesCompat.getDrawable(getResources(), R.drawable.curved_bg_selected, null)
            //
            binding?.byTodayText?.setTextColor(ResourcesCompat.getColor(getResources(), R.color.text_color_light, null))
            binding?.byMonthlyIntervlText?.setTextColor(ResourcesCompat.getColor(getResources(), R.color.colorWhite, null))
            loadFragment(ByMonthFragment())
        }
    }

    private fun loadFragment(fragment: Fragment) {
        val ft = childFragmentManager.beginTransaction()
        ft.replace(R.id.root_container, fragment)
        ft.addToBackStack(null)
        ft.commit()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun loadAd() {
//        moPubAdsManager.onSDKintialized(object : MoPubAdsManager.ISDKinitialize {
//            override fun onIntialized(isInit: Boolean) {
//                binding?.adFrame?.removeAllViews()
//                binding?.adFrame?.let { moPubAdsManager.loadNativeAds(it, Constants.MOPUB_NATIVE, MoPubAdsManager.NativeAdType.SMALL_TYPE) }
//            }
//        })
//
//        if (moPubAdsManager.isInitialized) {
//            binding?.adFrame?.removeAllViews()
//            binding?.adFrame?.let { moPubAdsManager.loadNativeAds(it, Constants.MOPUB_NATIVE, MoPubAdsManager.NativeAdType.SMALL_TYPE) }
//        }
        adsManager.loadNativeAd(requireActivity(),  binding?.adFrame, AdsManager.NativeAdType.SMALL_TYPE)
    }

    private fun initPieChart() {

        val data: MutableCollection<GraphData> = java.util.ArrayList()

        binding?.graph?.setMinValue(0f)
        binding?.graph?.setMaxValue(100f)
        binding?.graph?.setDevideSize(0.5f)
        binding?.graph?.setBackgroundShapeWidthInDp(5)
        binding?.graph?.setForegroundShapeWidthInDp(7)
        binding?.graph?.setShapeForegroundColor(ResourcesCompat.getColor(getResources(), R.color.text_color_light, null))
        binding?.graph?.setShapeBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.browser_actions_bg_grey, null))

        data.add(GraphData(72F, ResourcesCompat.getColor(getResources(), R.color.tab_color_orange, null)))
        binding?.graph?.setData(data)
    }


}