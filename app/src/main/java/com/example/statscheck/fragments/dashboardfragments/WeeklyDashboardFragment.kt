package com.example.statscheck.fragments.dashboardfragments

import android.app.usage.UsageStats
import android.app.usage.UsageStatsManager
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.RectF
import android.graphics.Typeface
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.ArrayMap
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.toBitmap
import androidx.core.graphics.drawable.toDrawable
import androidx.lifecycle.MutableLiveData
import androidx.palette.graphics.Palette
import com.anychart.AnyChart
import com.anychart.chart.common.dataentry.SingleValueDataSet
import com.anychart.enums.Anchor
import com.anychart.graphics.vector.Fill
import com.anychart.graphics.vector.SolidFill
import com.anychart.graphics.vector.text.HAlign
import com.anychart.graphics.vector.text.VAlign
import com.example.statscheck.Constants
import com.example.statscheck.R
import com.example.statscheck.acitvities.DetailScreenTimeActivity
import com.example.statscheck.databinding.FragmentDailyDashboardBinding
import com.example.statscheck.databinding.FragmentDashBoardBinding
import com.example.statscheck.databinding.FragmentWeeklyDashboardBinding
import com.example.statscheck.fragments.BaseFragment
import com.example.statscheck.fragments.mostusagefragments.ByTodayFragment
import com.example.statscheck.helpers.AppNameComparator
import com.example.statscheck.helpers.UsageTimeComparator
import com.example.statscheck.horizontalchart.BarItem
import com.example.statscheck.managers.AdsManager
import com.example.statscheck.managers.PreferenceManager
import com.ghanshyam.graphlibs.GraphData
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter
import com.github.mikephil.charting.formatter.PercentFormatter
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet
import com.github.mikephil.charting.interfaces.datasets.IRadarDataSet
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import com.iobits.identification.managers.MoPubAdsManager
import com.ultramegasoft.radarchart.RadarHolder
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.text.DecimalFormat
import java.util.*
import javax.inject.Inject


class WeeklyDashboardFragment : BaseFragment() {
    @Inject
    lateinit var preferencesManager: PreferenceManager

    @Inject
    lateinit var moPubAdsManager: MoPubAdsManager
    private var tf: Typeface? = null
    @Inject
    lateinit var adsManager: AdsManager
    private val TAG = "WeeklyDashboardFragment"
    private var _binding: FragmentWeeklyDashboardBinding? = null
    private val binding get() = _binding
    private var mUsageStatsManager: UsageStatsManager? = null
    private lateinit var mInflater: LayoutInflater
    private lateinit var mPm: PackageManager
    private val mPackageStats = ArrayList<UsageStats>()
    private val mPakageNames = ArrayList<String>()
    private val mPakagePercentage = ArrayList<String>()
    private val mAppLabelMap = ArrayMap<String, String>()
    private lateinit var mAppLabelComparator: AppNameComparator
    private val readyForset = MutableLiveData<Boolean>()
    private var noHistory: Boolean = false
    private var five: Boolean = false
    private var four: Boolean = false
    private var three: Boolean = false
    private var two: Boolean = false
    private var total_screen_time: Long = 0
    private var lessThan20: Int = 0
    private var lessThanHour: Int = 0
    private var lessThan40: Int = 0
    private var greaterHour: Int = 0
    private var lessThanZero: Int = 0
    private var mUsageTimeComparator= UsageTimeComparator()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        _binding = FragmentWeeklyDashboardBinding.inflate(inflater, container, false)


        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //because of animateLayoutChange there is crash of viewGroup . to remove that below line is used.
        binding?.root?.layoutTransition?.setAnimateParentHierarchy(false)
        //for delay load this fragment when app is open
        Handler(Looper.getMainLooper()).postDelayed(object : Runnable {
            override fun run() {
                init()
                fetchData()
                loadAd()
            }
        }, 2500)

    }

    fun setTextAndColor(size: Int, get: Int){
        if (size == 0){
            binding?.firstAppText?.text = mPakageNames.get(size)
            binding?.firstAppColor?.background = get.toDrawable()
            binding?.firstAppDetail?.visibility = View.VISIBLE
        }else if (size ==1){
            binding?.secondAppText?.text = mPakageNames.get(size)
            binding?.secondAppColor?.background = get.toDrawable()
            binding?.secondAppDetail?.visibility = View.VISIBLE

        }else if (size ==2){
            binding?.thirdAppText?.text = mPakageNames.get(size)
            binding?.thirdAppColor?.background = get.toDrawable()
            binding?.thirdAppDetail?.visibility = View.VISIBLE

        }else if (size ==3){
            binding?.fourthAppText?.text = mPakageNames.get(size)
            binding?.fourthAppColor?.background = get.toDrawable()
            binding?.fourthAppDetail?.visibility = View.VISIBLE

        }else if (size ==4){
            binding?.fifthAppText?.text = mPakageNames.get(size)
            binding?.fifthAppColor?.background = get.toDrawable()
            binding?.fifthAppDetail?.visibility = View.VISIBLE

        }
        binding?.graphsAppsDetail?.visibility = View.VISIBLE
    }
    private fun initFirstPieChart() {


        val data: MutableCollection<GraphData> = java.util.ArrayList()
        binding?.graph?.setMinValue(0f)
        binding?.graph?.setMaxValue(100f)
        binding?.graph?.setDevideSize(0.5f)
        binding?.graph?.setBackgroundShapeWidthInDp(10)
        binding?.graph?.setShapeForegroundColor(ResourcesCompat.getColor(getResources(), R.color.text_color_light, null))
        binding?.graph?.setShapeBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.browser_actions_bg_grey, null))


        val arrayList : ArrayList<Int> = ArrayList()

        var  palette =  Palette.generate(mPm.getApplicationIcon(mPackageStats.get(0).packageName).toBitmap())
        val default = 0x000000
        for (i in 0 until mPakagePercentage.size) {
            palette =  Palette.generate(mPm.getApplicationIcon(mPackageStats.get(i).packageName).toBitmap())
            arrayList.add(palette.getVibrantColor(default))
            setTextAndColor(i,arrayList.get(i))
        }

        var total =0F
        for (i in 0 until mPakagePercentage.size) {
            data.add(GraphData(mPakagePercentage.get(i).toFloat(), arrayList.get(i)))
            total = total.plus(mPakagePercentage.get(i).toFloat())
        }

        if (total<100){
            total =  100F -total
            Log.d(TAG, "initPieChart: $total")
            data.add(GraphData(total, ResourcesCompat.getColor(getResources(), R.color.colorAccent, null)))
        }
        binding?.graphCenterText?.text =  timeFormat(total_screen_time)
        binding?.graph?.setData(data)
    }

    private fun fetchTopNameAndPercentage() {
        if (mPackageStats.size > 5) {
            mAppLabelMap.get(mPackageStats[0].packageName)?.let { mPakageNames.add(it) }
            mAppLabelMap.get(mPackageStats[1].packageName)?.let { mPakageNames.add(it) }
            mAppLabelMap.get(mPackageStats[2].packageName)?.let { mPakageNames.add(it) }
            mAppLabelMap.get(mPackageStats[3].packageName)?.let { mPakageNames.add(it) }
            mAppLabelMap.get(mPackageStats[4].packageName)?.let { mPakageNames.add(it) }
            val percentage0 = (setTimeInFormat(mPackageStats[0].totalTimeInForeground).toDouble() / setTimeInFormat(total_screen_time)) * 100
            val percentage1 = (setTimeInFormat(mPackageStats[1].totalTimeInForeground).toDouble() / setTimeInFormat(total_screen_time)) * 100
            val percentage2 = (setTimeInFormat(mPackageStats[2].totalTimeInForeground).toDouble() / setTimeInFormat(total_screen_time)) * 100
            val percentage3 = (setTimeInFormat(mPackageStats[3].totalTimeInForeground).toDouble() / setTimeInFormat(total_screen_time)) * 100
            val percentage4 = (setTimeInFormat(mPackageStats[4].totalTimeInForeground).toDouble() / setTimeInFormat(total_screen_time)) * 100

            mPakagePercentage.add(DecimalFormat("##.##").format(percentage0))
            mPakagePercentage.add(DecimalFormat("##.##").format(percentage1))
            mPakagePercentage.add(DecimalFormat("##.##").format(percentage2))
            mPakagePercentage.add(DecimalFormat("##.##").format(percentage3))
            mPakagePercentage.add(DecimalFormat("##.##").format(percentage4))

            five = true
        } else if (mPackageStats.size > 4) {
            mAppLabelMap.get(mPackageStats[0].packageName)?.let { mPakageNames.add(it) }
            mAppLabelMap.get(mPackageStats[1].packageName)?.let { mPakageNames.add(it) }
            mAppLabelMap.get(mPackageStats[2].packageName)?.let { mPakageNames.add(it) }
            mAppLabelMap.get(mPackageStats[3].packageName)?.let { mPakageNames.add(it) }

            val percentage0 = (setTimeInFormat(mPackageStats[0].totalTimeInForeground).toDouble() / setTimeInFormat(total_screen_time)) * 100
            val percentage1 = (setTimeInFormat(mPackageStats[1].totalTimeInForeground).toDouble() / setTimeInFormat(total_screen_time)) * 100
            val percentage2 = (setTimeInFormat(mPackageStats[2].totalTimeInForeground).toDouble() / setTimeInFormat(total_screen_time)) * 100
            val percentage3 = (setTimeInFormat(mPackageStats[3].totalTimeInForeground).toDouble() / setTimeInFormat(total_screen_time)) * 100
            mPakagePercentage.add(DecimalFormat("##.##").format(percentage0))
            mPakagePercentage.add(DecimalFormat("##.##").format(percentage1))
            mPakagePercentage.add(DecimalFormat("##.##").format(percentage2))
            mPakagePercentage.add(DecimalFormat("##.##").format(percentage3))
            four = true
        } else if (mPackageStats.size > 3) {
            mAppLabelMap.get(mPackageStats[0].packageName)?.let { mPakageNames.add(it) }
            mAppLabelMap.get(mPackageStats[1].packageName)?.let { mPakageNames.add(it) }
            mAppLabelMap.get(mPackageStats[2].packageName)?.let { mPakageNames.add(it) }


            val percentage0 = (setTimeInFormat(mPackageStats[0].totalTimeInForeground).toDouble() / setTimeInFormat(total_screen_time)) * 100
            val percentage1 = (setTimeInFormat(mPackageStats[1].totalTimeInForeground).toDouble() / setTimeInFormat(total_screen_time)) * 100
            val percentage2 = (setTimeInFormat(mPackageStats[2].totalTimeInForeground).toDouble() / setTimeInFormat(total_screen_time)) * 100

            mPakagePercentage.add(DecimalFormat("##.##").format(percentage0))
            mPakagePercentage.add(DecimalFormat("##.##").format(percentage1))
            mPakagePercentage.add(DecimalFormat("##.##").format(percentage2))
            three = true
        } else if (mPackageStats.size > 2) {
            mAppLabelMap.get(mPackageStats[0].packageName)?.let { mPakageNames.add(it) }
            mAppLabelMap.get(mPackageStats[1].packageName)?.let { mPakageNames.add(it) }

            val percentage0 = (setTimeInFormat(mPackageStats[0].totalTimeInForeground).toDouble() / setTimeInFormat(total_screen_time)) * 100
            val percentage1 = (setTimeInFormat(mPackageStats[1].totalTimeInForeground).toDouble() / setTimeInFormat(total_screen_time)) * 100
            mPakagePercentage.add(DecimalFormat("##.##").format(percentage0))
            mPakagePercentage.add(DecimalFormat("##.##").format(percentage1))
            two = true
        } else {
            noHistory = true
        }
    }

    fun setTimeInFormat(totalTimeInForeground: Long): Int {
        val timeSec: Long = totalTimeInForeground / 1000
        var requiredFormat = 0
        val hours = timeSec.toInt() / 3600
        requiredFormat = hours //hh:mm:ss formatted string
        val temp = timeSec.toInt() - hours * 3600
        val mins = temp / 60
        if (hours==0 && mins >0){
            requiredFormat = 1
            Log.d(TAG, "setTimeInFormat: return $requiredFormat")
            return requiredFormat
        }
        return requiredFormat
    }

    fun fetchData() {
        //run on coroutine thread so everything done in line and app dosnt lag
        CoroutineScope(Dispatchers.IO).launch {
            getalldata()
            withContext(Dispatchers.Main) {
                setDataIntoChart()
            }
        }
    }

    private fun setDataIntoChart() {
        Collections.sort(mPackageStats, mUsageTimeComparator)

        fetchTopNameAndPercentage()
        binding?.screenTimeTxt?.text = timeFormat(total_screen_time)
        readyForset.value = false
        initFirstPieChart()
        binding?.horizontal?.init(context)?.hasAnimation(true)?.addAll(setHorizontalData())?.build()
        initPieTwo()
        setSecondPieChartData()

    }

    private fun setHorizontalData(): List<BarItem> {
        val themColor =ResourcesCompat.getColor(getResources(), R.color.theme_color_blue, null)
        val textColor =ResourcesCompat.getColor(getResources(), R.color.text_color_dark, null)
        val items: MutableList<BarItem> = java.util.ArrayList<BarItem>()

        items.add(BarItem(">1 hour", greaterHour.toDouble(),themColor, textColor))
        items.add(BarItem("30-60 mins", lessThanHour.toDouble(),themColor, textColor))
        items.add(BarItem("0-40 mins", lessThan40.toDouble(), themColor, textColor))
        items.add(BarItem("0-20 mins", lessThan20.toDouble(), themColor, textColor))
        items.add(BarItem("< 0 mins", lessThanZero.toDouble(), themColor, textColor))
        return items
    }

    fun init() {
        mUsageStatsManager = requireContext().getSystemService(AppCompatActivity.USAGE_STATS_SERVICE) as UsageStatsManager
        mInflater = requireContext().getSystemService(AppCompatActivity.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        mPm = requireContext().packageManager
    }


    private fun loadAd() {
//        moPubAdsManager.onSDKintialized(object : MoPubAdsManager.ISDKinitialize {
//            override fun onIntialized(isInit: Boolean) {
//                binding?.adFrame?.removeAllViews()
//                binding?.adFrame?.let { moPubAdsManager.loadNativeAds(it, Constants.MOPUB_NATIVE, MoPubAdsManager.NativeAdType.SMALL_TYPE) }
//            }
//        })
//
//        if (moPubAdsManager.isInitialized) {
//            binding?.adFrame?.removeAllViews()
//            binding?.adFrame?.let { moPubAdsManager.loadNativeAds(it, Constants.MOPUB_NATIVE, MoPubAdsManager.NativeAdType.SMALL_TYPE) }
//        }
        adsManager.loadNativeAd(requireActivity(),  binding?.adFrame, AdsManager.NativeAdType.MEDIUM_TYPE)
    }

    private fun initPieTwo() {
        binding?.PieChart?.setUsePercentValues(true)
        binding?.PieChart?.description?.isEnabled = false
        binding?.PieChart?.setExtraOffsets(5f, 10f, 5f, 5f)

        binding?.PieChart?.dragDecelerationFrictionCoef = 0.95f


        binding?.PieChart?.setCenterTextTypeface(Typeface.createFromAsset(requireContext().getAssets(), "helvetica_bold.ttf"))
//        binding?.PieChart?.centerText = generateCenterSpannableText()
        binding?.PieChart?.centerText =   timeFormat(total_screen_time)
        binding?.PieChart?.setCenterTextColor(ResourcesCompat.getColor(getResources(), R.color.text_color_light, null))
        binding?.PieChart?.setCenterTextSize(18F)

        binding?.PieChart?.setExtraOffsets(20f, 0f, 20f, 0f)

        binding?.PieChart?.isDrawHoleEnabled = true
        binding?.PieChart?.setHoleColor(ResourcesCompat.getColor(getResources(), R.color.grey, null))
//        binding?.PieChart?.setHoleColor(Color.LTGRAY)

        binding?.PieChart?.setTransparentCircleColor(Color.WHITE)
        binding?.PieChart?.setTransparentCircleAlpha(110)

        binding?.PieChart?.holeRadius = 58f
        binding?.PieChart?.transparentCircleRadius = 61f

        binding?.PieChart?.setDrawCenterText(true)

        binding?.PieChart?.rotationAngle = 0f
        // enable rotation of the chart by touch
        // enable rotation of the chart by touch
        binding?.PieChart?.isRotationEnabled = true
        binding?.PieChart?.isHighlightPerTapEnabled = true

        binding?.PieChart?.setOnChartValueSelectedListener(object : OnChartValueSelectedListener {
            override fun onValueSelected(e: Entry?, h: Highlight?) {
                if (e == null) return
                Log.e("VALSELECTED",
                        "Value: " + e.data + ", index: " + h!!.x
                                + ", DataSet index: " + h.dataIndex+"  " +h.x.toInt())
                val intent = Intent(getActivity(), DetailScreenTimeActivity::class.java)
                intent.putParcelableArrayListExtra("StatUsageList", mPackageStats)
                intent.putExtra("position", h.x.toInt())
                intent.putExtra("AppLabel", mPakageNames.get(h.x.toInt()))
                intent.putExtra("Percentage", mPakagePercentage.get(h.x.toInt()))
                Log.d("mPakagePercentage", "onValueSelected: "+mPakagePercentage.get(h.x.toInt()))
                startActivity(intent)
            }

            override fun onNothingSelected() {

            }
        })


        binding?.PieChart?.animateY(1400, Easing.EaseInOutQuad)
        // chart.spin(2000, 0, 360);

        // chart.spin(2000, 0, 360);
        val l = binding?.PieChart?.legend
        if (l != null) {
            l.verticalAlignment = Legend.LegendVerticalAlignment.TOP
            l.horizontalAlignment = Legend.LegendHorizontalAlignment.RIGHT
            l.orientation = Legend.LegendOrientation.VERTICAL
            l.setDrawInside(false)
            l.isEnabled = false
        }

    }


    private fun setSecondPieChartData() {
        val entries = ArrayList<PieEntry>()

        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
        // the chart.
        for (i in 0 until mPakagePercentage.size) {
            entries.add(PieEntry(mPakagePercentage.get(i).toFloat(), mPakageNames.get(i)))
        }
        val dataSet = PieDataSet(entries, "App Usages")
        dataSet.sliceSpace = 3f
        dataSet.selectionShift = 5f

        // add a lot of colors
        val colors = ArrayList<Int>()
//        colors.add(ResourcesCompat.getColor(getResources(), R.color.theme_color_blue, null))
//        colors.add(ResourcesCompat.getColor(getResources(), R.color.tab_color_orange, null))
//        colors.add(ResourcesCompat.getColor(getResources(), R.color.tab_color_green, null))
//        colors.add(ResourcesCompat.getColor(getResources(), R.color.tab_color_red, null))
//        colors.add(ResourcesCompat.getColor(getResources(), R.color.colorAccent, null))
//
        var  palette =  Palette.generate(mPm.getApplicationIcon(mPackageStats.get(0).packageName).toBitmap())
        val default = 0x000000
        for (i in 0 until mPakagePercentage.size) {
            palette =  Palette.generate(mPm.getApplicationIcon(mPackageStats.get(i).packageName).toBitmap())
            colors.add(palette.getVibrantColor(default))
        }
        dataSet.colors = colors
        dataSet.color
        //dataSet.setSelectionShift(0f);
        dataSet.valueLinePart1OffsetPercentage = 80f
        dataSet.valueLinePart1Length = 0.2f
        dataSet.valueLinePart2Length = 0.4f


//        dataSet.yValuePosition = PieDataSet.ValuePosition.OUTSIDE_SLICE
        val data = PieData(dataSet)
        data.setValueFormatter(PercentFormatter())
        data.setValueTextSize(11f)
        data.setValueTextColor(Color.WHITE)
        data.setValueTypeface(tf)
        binding?.PieChart?.data = data

        // undo all highlights
        binding?.PieChart?.highlightValues(null)
        binding?.PieChart?.invalidate()
    }


     fun getalldata() {
        val cal = Calendar.getInstance()
        cal.add(Calendar.DAY_OF_YEAR, -1)//-5 by default
        val stats = mUsageStatsManager?.queryUsageStats(UsageStatsManager.INTERVAL_WEEKLY,
                cal.timeInMillis, System.currentTimeMillis())
//        mUsageStatsManager?.queryAndAggregateUsageStats()

        val map = ArrayMap<String, UsageStats>()
        val statCount = stats!!.size

        for (i in 0 until statCount) {
            val pkgStats = stats[i]

            // load application labels for each application
            try {
                val appInfo = mPm.getApplicationInfo(pkgStats.packageName, 0)
                val label = appInfo.loadLabel(mPm).toString()
                if (!mAppLabelMap.contains(pkgStats.packageName)) {
                    mAppLabelMap[pkgStats.packageName] = label
                } else {
                    break
                }

                val existingStats = map[pkgStats.packageName]
                if (existingStats == null) {
                    val appUseTime = pkgStats.totalTimeInForeground / 1000
                    val neverUsedTime = 0

                    if (appUseTime > neverUsedTime) { //condition for add only those who used only
                        total_screen_time = total_screen_time.plus(pkgStats.totalTimeInForeground)
                        map[pkgStats.packageName] = pkgStats

                        lessThan20 = lessThan20.plus(getTimeIn20Mints(pkgStats.totalTimeInForeground))
                        lessThanHour = lessThanHour.plus(getTimeInLessThanHour(pkgStats.totalTimeInForeground))
                        lessThan40 = lessThan40.plus(getTimeIn40Mints(pkgStats.totalTimeInForeground))
                        greaterHour = greaterHour.plus(getTimeInGreaterThanHour(pkgStats.totalTimeInForeground))

                    }else{
                        lessThanZero = lessThanZero.plus(1)
                    }
                } else {
                    existingStats.add(pkgStats)
                }
            } catch (e: PackageManager.NameNotFoundException) {
                // This package may be gone.
            }
        }
//        mPackageStats.clear()
        mPackageStats.addAll(map.values)
        // Sort list
        mAppLabelComparator = AppNameComparator(mAppLabelMap)

    }

    fun getTimeIn20Mints(totalTimeInForeground: Long): Int{
        val timeSec: Long = totalTimeInForeground / 1000
        val hours = timeSec.toInt() / 3600
        var temp = timeSec.toInt() - hours * 3600
        val mins = temp / 60
        temp = temp - mins * 60
        val secs = temp
        if (hours<=0 && mins<20){
            if (mins>0){
                return  1
            }else if (secs> 0){
                return  1
            }else{
                return 0
            }
        }else{
            return 0
        }
    }
    fun getTimeIn40Mints(totalTimeInForeground: Long): Int{
        val timeSec: Long = totalTimeInForeground / 1000
        val hours = timeSec.toInt() / 3600
        var temp = timeSec.toInt() - hours * 3600
        val mins = temp / 60
        temp = temp - mins * 60
        val secs = temp
        if (hours<=0 && mins<40 && mins >20){
            return  1
        }else{
            return 0
        }
    }
    fun getTimeInLessThanHour(totalTimeInForeground: Long): Int{
        val timeSec: Long = totalTimeInForeground / 1000
        val hours = timeSec.toInt() / 3600
        var temp = timeSec.toInt() - hours * 3600
        val mins = temp / 60
        temp = temp - mins * 60
        val secs = temp
        if (hours<=0 && mins>40){
            return  1
        }else{
            return 0
        }
    }
    fun getTimeInGreaterThanHour(totalTimeInForeground: Long): Int{
        val timeSec: Long = totalTimeInForeground / 1000
        val hours = timeSec.toInt() / 3600
        var temp = timeSec.toInt() - hours * 3600
        val mins = temp / 60
        temp = temp - mins * 60
        val secs = temp
        if (hours>0){
            return  1
        }else{
            return 0
        }
    }


    fun timeFormat(totalTimeInForeground: Long): String {
        val timeSec: Long = totalTimeInForeground / 1000
        var requiredFormat = "0 sec"
        val hours = timeSec.toInt() / 3600
        var temp = timeSec.toInt() - hours * 3600
        val mins = temp / 60
        temp = temp - mins * 60
        val secs = temp
        if (mins > 0) {
            if (hours > 0) {
                requiredFormat = "$hours hr, $mins min" //hh:mm:ss formatted string
            }
            else {
                requiredFormat = "$mins min" //hh:mm:ss formatted string
            }
        }else if(hours > 0){
            requiredFormat = "$hours hr" //hh:mm:ss formatted string
        }
        return requiredFormat
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}