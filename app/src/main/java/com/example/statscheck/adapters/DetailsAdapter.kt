package com.example.statscheck.adapters

import android.app.usage.UsageStats
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.text.format.DateUtils
import android.util.ArrayMap
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.statscheck.R
import com.example.statscheck.acitvities.HistoryActivity
import java.text.DateFormat

class DetailsAdapter : RecyclerView.Adapter<DetailsAdapter.ViewHolder>() {

    private val TAG = "StatsAdapter"
    private lateinit var context: Context
    private var mPosition: Int = 0
    private lateinit var AppLabel: String
    private var mPackageStats: ArrayList<UsageStats> = ArrayList<UsageStats>()
    private var mPm: PackageManager? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.detail_stats_item, parent, false))
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // Bind the data efficiently with the holder
        val pkgStats = mPackageStats[position]

        val label = AppLabel
        val lastTimeUsed =DateUtils.formatSameDayTime(pkgStats.lastTimeUsed,
                System.currentTimeMillis(), DateFormat.MEDIUM, DateFormat.MEDIUM)
        val totalUsageTime=DateUtils.formatElapsedTime(pkgStats.totalTimeInForeground / 1000)

        holder.pkgName.text = label
        holder.lastTimeUsed.text = lastTimeUsed
        holder.usageTime.text = totalUsageTime

        Glide.with(context)
                .load(mPm?.getApplicationIcon(pkgStats.packageName)).into(holder.pkgIcon)

        holder.itemView.setOnClickListener {
            val intent = Intent(context, HistoryActivity::class.java)
            intent.putExtra("lastTimeUsed", lastTimeUsed)
            intent.putExtra("appName", label)
            intent.putExtra("totalUsageTime", totalUsageTime)
            intent.putExtra("packageName", pkgStats.packageName)
            context.startActivity(intent)
        }

    }


    override fun getItemCount(): Int {
        return mPackageStats.size
    }

    fun setData(AppLabel: String,
                mPackageStats1: ArrayList<UsageStats>,
                mPm1: PackageManager,
                mPosition: Int) {
        this.AppLabel = AppLabel
        mPackageStats = mPackageStats1
        mPm = mPm1
        this.mPosition = mPosition
        notifyDataSetChanged()
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val pkgIcon: ImageView = view.findViewById(R.id.pkgIcon)
        val pkgName: TextView = view.findViewById(R.id.package_name)
        val lastTimeUsed: TextView = view.findViewById(R.id.last_time_used)
        val usageTime: TextView = view.findViewById(R.id.usage_time)
    }


}
