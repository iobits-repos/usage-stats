package com.example.statscheck.adapters


import android.app.usage.UsageStats
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.util.ArrayMap
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.statscheck.R
import com.example.statscheck.acitvities.DetailScreenTimeActivity
import com.example.statscheck.helpers.AppNameComparator
import com.example.statscheck.helpers.LastTimeUsedComparator
import com.example.statscheck.helpers.UsageTimeComparator
import java.util.*

class StatsAdapter : RecyclerView.Adapter<StatsAdapter.ViewHolder>() {

    private val TAG = "StatsAdapter"
    private lateinit var context: Context
    private lateinit var intentContext: Context
    var onClick: ((Int) -> Unit)? = null
    private lateinit var mAppLabelMap: ArrayMap<String, String>
    private var mPackageStats: ArrayList<UsageStats> = ArrayList<UsageStats>()
    //    private var mStopDoubleName= ArrayList<String>()
    private var mPm: PackageManager? = null
    private lateinit var mInflater: LayoutInflater
    private var mAppLabelComparator: AppNameComparator? = null
    private var mLastTimeUsedComparator: LastTimeUsedComparator? = null
    private var mUsageTimeComparator: UsageTimeComparator? = null
    //
    private val _DISPLAY_ORDER_USAGE_TIME = 0
    private val _DISPLAY_ORDER_LAST_TIME_USED = 1
    private val _DISPLAY_ORDER_APP_NAME = 2
    private var mDisplayOrder = _DISPLAY_ORDER_USAGE_TIME

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        intentContext=parent.context
        this.context=parent.context
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item, parent, false))
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // Bind the data efficiently with the holder
        val pkgStats = mPackageStats[position]

        val label = mAppLabelMap.get(pkgStats.packageName)

            holder.pkgName.text = label
//            holder.lastTimeUsed.text = DateUtils.formatSameDayTime(pkgStats.lastTimeUsed,
//                    System.currentTimeMillis(), DateFormat.MEDIUM, DateFormat.MEDIUM)
//            holder.usageTime.text = DateUtils.formatElapsedTime(pkgStats.totalTimeInForeground / 1000)
            holder.usageTime.text = setTimeInFormat(pkgStats.totalTimeInForeground)
            holder.pkgIcon.let {
                context.let { it1 ->
                    Glide.with(it1)
                            .load(mPm?.getApplicationIcon(pkgStats.packageName))
                            .into(it)
                }
            }

//        holder.itemView.setOnClickListener {
//            val intent = Intent(intentContext, DetailScreenTimeActivity::class.java)
//            intent.putParcelableArrayListExtra("StatUsageList",mPackageStats)
//            intent.putExtra("position",position)
//            intent.putExtra("AppLabel",label)
//            intentContext.startActivity(intent)
//        }
        holder.itemView.setOnClickListener {
            onClick?.invoke(position)
        }

    }

    fun setTimeInFormat(totalTimeInForeground: Long): String {
        val timeSec: Long = totalTimeInForeground/1000
        var requiredFormat="0 sec"
        val hours = timeSec.toInt() / 3600
        var temp = timeSec.toInt() - hours * 3600
        val mins = temp / 60
        temp = temp - mins * 60
        val secs = temp
        if (secs>0){
            if (mins>0){
                if (hours>0){
                     requiredFormat = "$hours hr, $mins min, $secs sec" //hh:mm:ss formatted string
                }else{
                    requiredFormat =  "$mins min, $secs sec" //hh:mm:ss formatted string
                }
            }else if(hours>0){
                requiredFormat = "$hours hr" //hh:mm:ss formatted string
            }else{
                requiredFormat =  "$secs sec" //hh:mm:ss formatted string
            }
        }else if(hours>0){
            requiredFormat = "$hours hr, $mins min" //hh:mm:ss formatted string
        }else if (mins >0){
            requiredFormat =  "$mins min" //hh:mm:ss formatted string
        }
        return requiredFormat
    }

    override fun getItemCount(): Int {
        return mPackageStats.size
    }

    fun setData(
                mAppLabelMap1: ArrayMap<String, String>,
                mPackageStats1: ArrayList<UsageStats>,
                mPm1: PackageManager, mInflater1: LayoutInflater,
                mAppLabelComparator1: AppNameComparator,
                mLastTimeUsedComparator1: LastTimeUsedComparator,
                mUsageTimeComparator1: UsageTimeComparator) {

//        context = context1
        mAppLabelMap = mAppLabelMap1
        mPackageStats = mPackageStats1
        mPm = mPm1
        mInflater = mInflater1
        mAppLabelComparator = mAppLabelComparator1
        mLastTimeUsedComparator = mLastTimeUsedComparator1
        mUsageTimeComparator = mUsageTimeComparator1
        notifyDataSetChanged()
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val pkgIcon: ImageView = view.findViewById(R.id.pkgIcon)
        val pkgName: TextView = view.findViewById(R.id.package_name)
//        val lastTimeUsed: TextView = view.findViewById(R.id.last_time_used)
        val usageTime: TextView = view.findViewById(R.id.usage_time)
    }

    fun sortList(sortOrder: Int) {
        if (mDisplayOrder == sortOrder) {
            // do nothing
            return
        }
        mDisplayOrder = sortOrder
        sortList()
    }


    fun sortList() {
        if (mDisplayOrder == _DISPLAY_ORDER_USAGE_TIME) {
            Collections.sort(mPackageStats, mUsageTimeComparator)
        } else if (mDisplayOrder == _DISPLAY_ORDER_LAST_TIME_USED) {
            Collections.sort(mPackageStats, mLastTimeUsedComparator)
        } else if (mDisplayOrder == _DISPLAY_ORDER_APP_NAME) {
            Collections.sort(mPackageStats, mAppLabelComparator)
        }
        notifyDataSetChanged()
    }
    fun removeAll(list: ArrayList<UsageStats>){
        mPackageStats.removeAll(list)
        notifyDataSetChanged()
    }


//    override fun getItemId(position: Int): Long {
//        return position.toLong()
//    }
//
//    override fun getItemViewType(position: Int): Int {
//        return position
//    }
}
