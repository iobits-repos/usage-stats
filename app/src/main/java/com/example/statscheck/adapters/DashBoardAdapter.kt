package com.example.statscheck.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.statscheck.fragments.dashboardfragments.DailyDashboardFragment
import com.example.statscheck.fragments.dashboardfragments.WeeklyDashboardFragment

class DashBoardAdapter(fragmentActivity: FragmentActivity) : FragmentStateAdapter(fragmentActivity) {

    override fun getItemCount(): Int = 5

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> DailyDashboardFragment()
            1 -> WeeklyDashboardFragment()
            else -> WeeklyDashboardFragment()
        }
    }
}