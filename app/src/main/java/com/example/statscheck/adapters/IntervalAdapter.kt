package com.example.statscheck.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.statscheck.fragments.intervalfragments.DailyFragment
import com.example.statscheck.fragments.intervalfragments.MonthlyFragment
import com.example.statscheck.fragments.intervalfragments.WeeklyFragment

class IntervalAdapter(fragmentActivity: FragmentActivity) : FragmentStateAdapter(fragmentActivity) {

    override fun getItemCount(): Int = 5

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> WeeklyFragment()
            1 -> DailyFragment()
            2 -> MonthlyFragment()
            else -> DailyFragment()
        }
    }
}