package com.example.statscheck.adapters

import android.app.usage.UsageStats
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.text.format.DateUtils
import android.util.ArrayMap
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.statscheck.R
import com.example.statscheck.acitvities.HistoryActivity
import java.text.DateFormat

class MostUsageAdapter : RecyclerView.Adapter<MostUsageAdapter.ViewHolder>() {
    private lateinit var mAppLabelMap: ArrayMap<String, String>
    private val TAG = "StatsAdapter"
    private lateinit var context: Context
    private var mPackageStats: ArrayList<UsageStats> = ArrayList<UsageStats>()
    private var mPm: PackageManager? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item, parent, false))
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // Bind the data efficiently with the holder
        val pkgStats = mPackageStats[position]
        val lastTimeUsed =DateUtils.formatSameDayTime(pkgStats.lastTimeUsed,
                System.currentTimeMillis(), DateFormat.MEDIUM, DateFormat.MEDIUM)
        val totalUsageTime=setTimeInFormat(pkgStats.totalTimeInForeground)
        val label = mAppLabelMap.get(pkgStats.packageName)

        holder.pkgName.text = label
        holder.usageTime.text = totalUsageTime
        holder.usageTime.setTextColor(ResourcesCompat.getColor(context.getResources(), R.color.tab_color_red, null))
        holder.pkgIcon.let {
            context.let { it1 ->
                Glide.with(it1)
                        .load(mPm?.getApplicationIcon(pkgStats.packageName))
                        .into(it)
            }
        }
        holder.itemView.setOnClickListener {
            val intent = Intent(context, HistoryActivity::class.java)
            intent.putExtra("lastTimeUsed", lastTimeUsed)
            intent.putExtra("appName", label)
            intent.putExtra("fromMostUsage", true)
            intent.putExtra("totalUsageTime", DateUtils.formatElapsedTime(pkgStats.totalTimeInForeground / 1000))
            intent.putExtra("packageName", pkgStats.packageName)

            context.startActivity(intent)
        }
    }


    override fun getItemCount(): Int {
        return mPackageStats.size
    }

    fun setData(mAppLabelMap1: ArrayMap<String, String>,
                mPackageStats1: ArrayList<UsageStats>,
                mPm1: PackageManager) {
       mAppLabelMap = mAppLabelMap1
        mPackageStats = mPackageStats1
        mPm = mPm1
        notifyDataSetChanged()
    }
    fun setTimeInFormat(totalTimeInForeground: Long): String {
        val timeSec: Long = totalTimeInForeground/1000
        var requiredFormat="0 sec"
        val hours = timeSec.toInt() / 3600
        var temp = timeSec.toInt() - hours * 3600
        val mins = temp / 60
        temp = temp - mins * 60
        val secs = temp
        if (secs>0){
            if (mins>0){
                if (hours>0){
                    requiredFormat = "$hours hr, $mins min, $secs sec" //hh:mm:ss formatted string
                }else{
                    requiredFormat =  "$mins min, $secs sec" //hh:mm:ss formatted string
                }
            }else if(hours>0){
                requiredFormat = "$hours hr" //hh:mm:ss formatted string
            }else{
                requiredFormat =  "$secs sec" //hh:mm:ss formatted string
            }
        }else if(hours>0){
            requiredFormat = "$hours hr, $mins min" //hh:mm:ss formatted string
        }else if (mins >0){
            requiredFormat =  "$mins min" //hh:mm:ss formatted string
        }
        return requiredFormat
    }
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val pkgIcon: ImageView = view.findViewById(R.id.pkgIcon)
        val pkgName: TextView = view.findViewById(R.id.package_name)
        val usageTime: TextView = view.findViewById(R.id.usage_time)
    }


}
