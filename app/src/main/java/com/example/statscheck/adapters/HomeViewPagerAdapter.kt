package com.example.statscheck.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.statscheck.fragments.BoostFragment
import com.example.statscheck.fragments.DashBoardFragment
import com.example.statscheck.fragments.StatsFragment

class HomeViewPagerAdapter(fragmentActivity: FragmentActivity) : FragmentStateAdapter(fragmentActivity) {

    override fun getItemCount(): Int = 3

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> DashBoardFragment()
            1 -> StatsFragment()
            2 -> BoostFragment()
            else -> StatsFragment()
        }
    }
}