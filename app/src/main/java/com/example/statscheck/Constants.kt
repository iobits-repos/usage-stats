package com.example.statscheck

interface Constants {
    companion object {
        const val URL_BASE_MUSIC = ""
        const val APP_LINK = "appLink"
        const val ACTION_TYPE = "action"
        const val ACTION_TYPE_LIKE = "like"
        const val ACTION_TYPE_COMMENT = "comment"
        const val TOOL_NONE = "TOOL_NONE"
        const val TOOL_STYLE = "TOOL_STYLE"
        const val TOOL_ADDS_ON = "TOOL_ADDS_ON"
        const val TOOL_MAKE_OVER = "TOOL_MAKE_OVER"
        const val TOOL_CREATION = "TOOL_CREATION"
        const val FOLDER_NAME = "SmartyMen"


        // Google billing keys
        const val PLAY_BILLING_LICENSE_KEY = ""
        const val MERCHANT_ID = ""
        const val ITEM_SKU = "android.test.purchased"
        const val PRODUCT_ID = "sub_monthly"

        const val SIX_MONTHS_PRODUCT_ID = "six_months_id"
        const val ONE_YEAR_PRODUCT_ID = "one_year_id"
        const val ONE_MONTH_PRODUCT_ID = "one_month_id"

        const val USERS_ACTIVITY_TITLE = "USERS_ACTIVITY_TITLE"
        const val USERS_ACTIVITY_TYPE = "USERS_ACTIVITY_TYPE"
        const val LIKES_EXTRA_KEY = "LIKES_EXTRA_KEY"
        const val FOLLOWERS_EXTRA_KEY = "FOLLOWERS_EXTRA_KEY"
        const val FOLLOWING_EXTRA_KEY = "FOLLOWING_EXTRA_KEY"
        const val POST_ID_EXTRA_KEY = "POST_ID_EXTRA_KEY"
        const val POST_DETAILS_FROM_EXTRA_KEY = "POST_DETAILS_FROM_EXTRA_KEY"
        const val USER_ID = "USER_ID"
        const val NUMBER_OF_ON_BOARDING_SLIDER = 5
        const val IMAGE_URL_EXTRA = "IMAGE_URL_EXTRA"
        const val LOGIN_RESULT_EXTRA_KEY = "LOGIN_RESULT_EXTRA_KEY"
        const val NEW_POST_LOCAL_EVENT = "NEW_POST_LOCAL_EVENT"
        const val REFRESH_POST_EXTRA = "REFRESH_POST_EXTRA"
        const val DRAWER_UPDATE_EVENT = "DRAWER_UPDATE_EVENT"
        const val REFRESH_DRAWER_EXTRA = "REFRESH_DRAWER_EXTRA"
        const val SCREEENTIME_DAILY = "SCREEENTIME_DAILY"
        const val SCREEENTIME_MONTHLY = "SCREEENTIME_MONTHLY"
        const val SCREEENTIME_WEEKLY = "SCREEENTIME_WEEKLY"
        const val SCREEENTIME_DRAWER = "SCREEENTIME_DRAWER"

        const val chabi = ""


        //Mopub Text IDs
        const val MOPUB_INTERSTITIAL = "24534e1901884e398f1253216226017e"
        const val MOPUB_BANNER = "b195f8dd8ded45fe847ad89ed1d016da"
        const val MOPUB_NATIVE = "11a17b188668469fb0412708c3d16813"

        //ADMob Test Ids
        const val ADMOD_INTERSTITIAL = "ca-app-pub-3940256099942544/1033173712"
        const val ADMOD_NATIVE = "ca-app-pub-3940256099942544/2247696110"
        const val ADMOD_BANNER = "ca-app-pub-3940256099942544/6300978111"

        //FACEBOOK Test Ids
        const val FACEBOOK_INTERSTITIAL = "YOUR_PLACEMENT_ID"

        //---------------------Actual Ads------------------------//

        /*  //Mopub Actual IDS
          const val MOPUB_INTERSTITIAL = "bab3e26ec3084a7a87e0eed6b55be49b";
          const val MOPUB_BANNER = "405105adde194deb886ad759191ebaa8";
          const val MOPUB_NATIVE = "4231bd3fbd1e4a1e9db833554f2ea65a";

          //ADMob Actual ids
          const val ADMOD_INTERSTITIAL = "ca-app-pub-4208847408353022/1783610912";
          const val ADMOD_NATIVE = "ca-app-pub-4208847408353022/7945145844";
          const val ADMOD_BANNER = "ca-app-pub-4208847408353022/5722855922";

          //FACEBOOK Actual Ids
          const val FACEBOOK_INTERSTITIAL = "249043193360666_249043433360642";
  */

    }

}