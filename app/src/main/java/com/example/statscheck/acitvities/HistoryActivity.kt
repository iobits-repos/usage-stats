package com.example.statscheck.acitvities

import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.core.content.res.ResourcesCompat
import com.bumptech.glide.Glide
import com.example.statscheck.Constants
import com.example.statscheck.R
import com.example.statscheck.databinding.ActivityDetailScreenTimeBinding
import com.example.statscheck.databinding.ActivityHistoryBinding
import com.example.statscheck.managers.AdsManager
import com.example.statscheck.managers.PreferenceManager
import com.iobits.identification.managers.MoPubAdsManager
import javax.inject.Inject

class HistoryActivity : BaseActivity() {
    private lateinit var binding: ActivityHistoryBinding
    private var lastTimeUsed: String = ""
    private var appName: String = ""
    private var totalUsageTime: String = ""
    private var pkgName: String = ""
    private var isFromMostUsage: Boolean = false
    @Inject
    lateinit var preferencesManager: PreferenceManager
    @Inject
    lateinit var moPubAdsManager: MoPubAdsManager
    @Inject
    lateinit var adsManager: AdsManager
    private var mPm: PackageManager? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        binding = ActivityHistoryBinding.inflate(layoutInflater)
        setContentView(binding.root)
//        setContentView(R.layout.activity_history)
        loadAd()
        init()
    }

    private fun init() {
        appName = intent.getStringExtra("appName").toString()
        totalUsageTime = intent.getStringExtra("totalUsageTime").toString()
        lastTimeUsed = intent.getStringExtra("lastTimeUsed").toString()
        pkgName = intent.getStringExtra("packageName").toString()
        isFromMostUsage = intent.getBooleanExtra("fromMostUsage",false)

        binding.toolbar.toolbarTitleTextView.text = "History"
        binding.toolbar.historyIcon.visibility = View.VISIBLE
        binding.toolbar.miniIcon.visibility = View.VISIBLE
        mPm = packageManager
        Glide.with(this)
                .load(mPm?.getApplicationIcon(pkgName))
                .into(binding.toolbar.historyIcon)

        binding.appNameTxt.text = appName

        binding.lastTimeUsed.text = lastTimeUsed
        binding.totalUsageTime.text = totalUsageTime
        binding.applicationName.text = appName

        binding.toolbar.backArrow.setOnClickListener {
            onBackPressed()
        }

        if(isFromMostUsage){
            binding.mostUsageAppAnimation.visibility = View.VISIBLE
            binding.clockGif.visibility = View.GONE
            binding.totalUsageTime.setTextColor(ResourcesCompat.getColor(getResources(), R.color.tab_color_red, null))
        }
    }
    private fun loadAd() {
//        moPubAdsManager.onSDKintialized(object : MoPubAdsManager.ISDKinitialize {
//            override fun onIntialized(isInit: Boolean) {
//                binding.adFrame.removeAllViews()
//                moPubAdsManager.loadNativeAds(binding.adFrame, Constants.MOPUB_NATIVE, MoPubAdsManager.NativeAdType.MEDIUM_TYPE)
//            }
//        })
//        if (moPubAdsManager.isInitialized) {
//            binding.adFrame.removeAllViews()
//            moPubAdsManager.loadNativeAds(binding.adFrame, Constants.MOPUB_NATIVE, MoPubAdsManager.NativeAdType.MEDIUM_TYPE)
//        }
        adsManager.loadNativeAd(this, binding.adFrame, AdsManager.NativeAdType.MEDIUM_TYPE)
        binding.adFramCard.visibility = View.VISIBLE
    }

    override fun onBackPressed() {
        adsManager.showInterstitialAd(this)
        super.onBackPressed()
//        moPubAdsManager.showMoPubInterstitial()
    }

}