package com.example.statscheck.acitvities;

import android.app.AppOpsManager;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentTransaction;

import com.example.statscheck.Constants;
import com.example.statscheck.R;
import com.example.statscheck.fragments.SplashFragment;
import com.example.statscheck.managers.AdsManager;
import com.example.statscheck.managers.BillingManager;
import com.example.statscheck.managers.PreferenceManager;
import com.iobits.identification.managers.MoPubAdsManager;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.inject.Inject;

public class SplashActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = "SplashActivity";
    private FrameLayout container;
    //    MoPubAdsManager moPubAdsManager;
//    PreferenceManager preferenceManager;
    @Inject
    MoPubAdsManager moPubAdsManager;
    @Inject
    AdsManager adsManager;
    @Inject
    PreferenceManager preferencesManager;

    @Inject
    BillingManager billingManager;
    SplashFragment splashFragment;
    RelativeLayout permissionLayout;
//    View backgorundView;
    CardView grantAccessBtn;
    TextView start;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppTheme);
        setContentView(R.layout.activity_splash);

        init();
        loadAd();

        if (getIntent().getExtras() != null) {
            String appLink = getIntent().getExtras().getString(Constants.APP_LINK);
            if (appLink != null) {
                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(appLink));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                    startActivity(intent);
                    finish();
                } catch (final Exception ignored) {
                }
            }

        }

    }

    private void loadAd() {
//        moPubAdsManager.onSDKintialized(isInit -> {
//            container.removeAllViews();
//            moPubAdsManager.loadNativeAds(container, Constants.MOPUB_NATIVE, MoPubAdsManager.NativeAdType.REGULAR_TYPE);
////            Log.d(TAG, "subscribeForSdkInit: ");
//        });
//
//        if (moPubAdsManager.isInitialized()) {
//            removeFragment();
//            container.removeAllViews();
//            moPubAdsManager.loadNativeAds(container, Constants.MOPUB_NATIVE, MoPubAdsManager.NativeAdType.REGULAR_TYPE);
////            Log.d(TAG, "sdk already initialized and native ad is loading : ");
//        }
        adsManager.loadNativeAd(this, container, AdsManager.NativeAdType.REGULAR_TYPE);

    }

    private void init() {
        container = findViewById(R.id.adFrame);
        splashFragment = new SplashFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.adFrame, splashFragment).commit();


//        backgorundView = findViewById(R.id.backgorundView);
        permissionLayout = findViewById(R.id.permissionLayout);
        grantAccessBtn = findViewById(R.id.grantAccessBtn);
        grantAccessBtn.setOnClickListener(this);
        start = findViewById(R.id.start);
        start.setOnClickListener(this);

        if (requestPermissions()) {
//            backgorundView.setVisibility(View.GONE);
            permissionLayout.setVisibility(View.GONE);
        }

    }


    public String giveDate() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("EEE, d MMM, yyyy");
        return sdf.format(cal.getTime());
    }

    private void openMainActivity() {
//        if (!preferencesManager.getBoolean(IS_APP_FIRST_RUN,false)){
//            startActivity(new Intent(SplashActivity.this, SlidersActivity.class));
//            finish();
//        }
//        else{
        final Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashActivity.this, UsageStatsActivity.class);
                intent.putExtra("fromSplash",true);
                startActivity(intent);
//                startActivity(new Intent(SplashActivity.this, UsageStatsActivity.class));
                finish();
            }
        }, 250);

//        }
    }

    private void removeFragment() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (splashFragment != null) {
            transaction.remove(splashFragment);
            transaction.commit();
            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
            splashFragment = null;
        }
    }

    private boolean requestPermissions() {
        boolean granted;
        AppOpsManager appOps = (AppOpsManager) this
                .getSystemService(Context.APP_OPS_SERVICE);
        int mode = appOps.checkOpNoThrow(AppOpsManager.OPSTR_GET_USAGE_STATS,
                android.os.Process.myUid(), getPackageName());

        if (mode == AppOpsManager.MODE_DEFAULT) {
            granted = (checkCallingOrSelfPermission(android.Manifest.permission.PACKAGE_USAGE_STATS) == PackageManager.PERMISSION_GRANTED);
        } else {
            granted = (mode == AppOpsManager.MODE_ALLOWED);
        }
        return granted;

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.start:
                if (requestPermissions()) {
                    showToast("We are fetching your usage stats. Please Wait!");
                  /*  throw new RuntimeException("Test Crash"); // Force a crash*/
                    openMainActivity();
//                    Log.d("showToast", "onClick: showToast");
                } else {
//                    backgorundView.setVisibility(View.VISIBLE);
                    permissionLayout.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.grantAccessBtn:
                startActivity(new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS));
//                backgorundView.setVisibility(View.GONE);
                permissionLayout.setVisibility(View.GONE);
                break;
        }
    }

}




