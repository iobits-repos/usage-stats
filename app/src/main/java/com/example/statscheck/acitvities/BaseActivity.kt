package com.example.statscheck.acitvities

import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

abstract class BaseActivity : DaggerAppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_base)
    }

    fun showToast(msg: String  ) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show()
    }

    fun showSnackBar(msg: String){
        val snackbar = Snackbar.make(findViewById<View>(android.R.id.content), "" + msg, Snackbar.LENGTH_LONG)
        snackbar.show()
    }


}