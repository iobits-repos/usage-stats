package com.example.statscheck.acitvities

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.Uri
import android.os.Bundle
import android.os.SystemClock
import android.view.View
import android.view.WindowManager
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.example.statscheck.Constants
import com.example.statscheck.R
import com.example.statscheck.adapters.HomeViewPagerAdapter
import com.example.statscheck.databinding.ActivityStatsBinding
import com.example.statscheck.databinding.DrawerHeaderProfileViewBinding
import com.example.statscheck.databinding.PremiumDilaogBinding
import com.example.statscheck.fragments.BoostFragment
import com.example.statscheck.fragments.DashBoardFragment
import com.example.statscheck.fragments.StatsFragment
import com.example.statscheck.managers.AdsManager
import com.example.statscheck.managers.BillingManager
import com.example.statscheck.managers.PreferenceManager
import com.gauravk.bubblenavigation.BubbleNavigationConstraintView
import com.iobits.identification.managers.MoPubAdsManager
import getDrawableKtx
import java.util.*
import javax.inject.Inject

class UsageStatsActivity : BaseActivity() {

    //    lateinit var billingManager: BillingManager
    private lateinit var adapter: HomeViewPagerAdapter

    @Inject
    lateinit var billingManager: BillingManager
    @Inject
    lateinit var adsManager: AdsManager
    @Inject
    lateinit var preferencesManager: PreferenceManager

    @Inject
    lateinit var moPubAdsManager: MoPubAdsManager

    var mLastClickTime = 0L
    var selectedItem = Constants.ONE_MONTH_PRODUCT_ID
    lateinit var headerBinding: DrawerHeaderProfileViewBinding

    //    lateinit var preferencesManager: PreferenceManager
    private val TAG = "UsageStatsActivity"
    private lateinit var top_navigation_constraint: BubbleNavigationConstraintView
    private lateinit var binding: ActivityStatsBinding
    private var fromSplash = false
    lateinit var broadCastListener: BroadcastReceiver

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        binding = ActivityStatsBinding.inflate(layoutInflater)
        setContentView(binding.root)
//        setContentView(R.layout.activity_stats)

        fromSplash = intent.getBooleanExtra("fromSplash", false)
        checkSubscribedProducts()
        showInAppDialog()
        registerBroadCastReceiver()//register broadcast for drawer update
//        Handler(Looper.getMainLooper()).postDelayed(object : Runnable {
//            override fun run() {
        init()
        initDrawer()
//            }
//        },1500)


    }

    private fun initDrawer() {
        headerBinding = DrawerHeaderProfileViewBinding.inflate(layoutInflater)
        binding.navView.addHeaderView(headerBinding.root)
    }

    private fun init() {
        if (!preferencesManager.getBoolean(PreferenceManager.Key.IS_APP_ADS_FREE, false)) {
            adsManager.loadInterstitialAd(this)
        }
        adapter = HomeViewPagerAdapter(this@UsageStatsActivity)
        binding.viewpager.offscreenPageLimit = 5
        binding.viewpager.isUserInputEnabled = false
        binding.viewpager.adapter = this.adapter
        binding.viewpager.setCurrentItem(1)
        binding.navView.itemIconTintList = null
        binding.toolbar.drawerIcon.setOnClickListener {
            if (!binding.drawerLayout.isDrawerOpen(GravityCompat.START))
                binding.drawerLayout.openDrawer(GravityCompat.START)
        }
        binding.toolbar.goPro.setOnClickListener {
            fromSplash = false
            showInAppDialog()
        }
        binding.navView.setNavigationItemSelectedListener { menuItem ->
            when (menuItem.itemId) {
                R.id.menu_item_go_premium -> {
                    fromSplash = false
                    showInAppDialog()
                }
                R.id.menu_share_with_friends -> shareApp()
                R.id.menu_privacy_policy -> privacyPolicy()
            }
            binding.drawerLayout.closeDrawer(GravityCompat.START)
            false
        }

//        loadFragment(StatsFragment())
        top_navigation_constraint = findViewById(R.id.top_navigation_constraint)
        top_navigation_constraint.setNavigationChangeListener { _, position ->
            binding.viewpager.setCurrentItem(position)
//            mLastClickTime = SystemClock.elapsedRealtime()
//             if (position==0){
//                 loadFragment(DashBoardFragment())
//             }else if (position==1){
//                 loadFragment(StatsFragment())
//             }else if (position==2){
//                 loadFragment(BoostFragment())
//             }
        }
/*
        // check if user click again and again before data loading
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1500){
            showSnackBar("please Wait Data is loading")
            return@setNavigationChangeListener
        }
        mLastClickTime = SystemClock.elapsedRealtime()*/
    }


//    private fun loadFragment(fragment: Fragment) {
//        val ft: FragmentTransaction = supportFragmentManager.beginTransaction()
//        ft.replace(R.id.root_container, fragment)
//        ft.addToBackStack(null)
//        ft.commit()
//    }



    private fun privacyPolicy() {
        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://docs.google.com/document/d/14CQcIV9LRlkxUNQzwEBZlI2PdmTtXFOmoRi42c0FXXs/edit")))
    }

    private fun shareApp() {
        try {
            val sendIntent = Intent()
            sendIntent.action = Intent.ACTION_SEND
            sendIntent.putExtra(Intent.EXTRA_TEXT,
                    "Want to find Time spend on Differents Apps???\n" +
                            "Try this App for check Total time spend on Apps in your mobile. https://play.google.com/store/apps/details?id=com.ss.stats.action")
            sendIntent.type = "text/plain"
            startActivity(sendIntent)
        } catch (e: Exception) {

        }
    }

    private fun showInAppDialog() {
//        var selectedItem = Constants.ONE_MONTH_PRODUCT_ID
        val inAppBinding: PremiumDilaogBinding = PremiumDilaogBinding.inflate(layoutInflater)

        val dialogBuilder = AlertDialog.Builder(this, android.R.style.Theme_DeviceDefault_Light_NoActionBar_Fullscreen)
        dialogBuilder.setCancelable(false)
        val premiumDialog = dialogBuilder.create()
        val window = premiumDialog.window
        window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)
        premiumDialog.setView(inAppBinding.root)


        inAppBinding.dealsView.sixMonthView.setOnClickListener {

            inAppBinding.dealsView.bestValueStart.visibility = View.VISIBLE
            inAppBinding.dealsView.bestValueEnd.visibility = View.GONE
            inAppBinding.dealsView.bestValueMiddle.visibility = View.GONE

            //
            inAppBinding.dealsView.sixMonthView.background = getDrawableKtx(R.drawable.deal_selected_bg)
            inAppBinding.dealsView.oneMonthView.background = getDrawableKtx(R.drawable.deal_unselected_bg)
            inAppBinding.dealsView.oneYearView.background = getDrawableKtx(R.drawable.deal_unselected_bg)
            selectedItem = Constants.SIX_MONTHS_PRODUCT_ID
        }
        inAppBinding.dealsView.oneYearView.setOnClickListener {
            inAppBinding.dealsView.bestValueStart.visibility = View.GONE
            inAppBinding.dealsView.bestValueEnd.visibility = View.VISIBLE
            inAppBinding.dealsView.bestValueMiddle.visibility = View.GONE
            //
            inAppBinding.dealsView.sixMonthView.background = getDrawableKtx(R.drawable.deal_unselected_bg)
            inAppBinding.dealsView.oneMonthView.background = getDrawableKtx(R.drawable.deal_unselected_bg)
            inAppBinding.dealsView.oneYearView.background = getDrawableKtx(R.drawable.deal_selected_bg)
            selectedItem = Constants.ONE_YEAR_PRODUCT_ID
        }
        inAppBinding.dealsView.oneMonthView.setOnClickListener {
            inAppBinding.dealsView.bestValueStart.visibility = View.GONE
            inAppBinding.dealsView.bestValueEnd.visibility = View.GONE
            inAppBinding.dealsView.bestValueMiddle.visibility = View.VISIBLE
            //
            inAppBinding.dealsView.sixMonthView.background = getDrawableKtx(R.drawable.deal_unselected_bg)
            inAppBinding.dealsView.oneYearView.background = getDrawableKtx(R.drawable.deal_unselected_bg)
            inAppBinding.dealsView.oneMonthView.background = getDrawableKtx(R.drawable.deal_selected_bg)
            selectedItem = Constants.ONE_MONTH_PRODUCT_ID
        }

        inAppBinding.crossIv.setOnClickListener {
            if (premiumDialog.isShowing) {
                premiumDialog.dismiss()
            }
            if (!fromSplash) {
                adsManager.showInterstitialAd(this)
//                moPubAdsManager.showMoPubInterstitial()
            }
        }
        inAppBinding.freeTrialTv.setOnClickListener {
            if (premiumDialog.isShowing) {
                premiumDialog.dismiss()
            }
            if (!fromSplash) {
                adsManager.showInterstitialAd(this)
//                moPubAdsManager.showMoPubInterstitial()
            }
        }

        inAppBinding.upgradeBtn.setOnClickListener { subscribeProduct(selectedItem) }

        if (premiumDialog.window != null) premiumDialog.window?.attributes?.windowAnimations = R.style.SlidingDialogAnimation

        premiumDialog.setCancelable(false)
        premiumDialog.show()
    }

    //To subscribe a specific product. we can't test subscription feature.
    private fun subscribeProduct(productId: String) {
        billingManager.subscribe(this, productId) { isSuccessful ->
            if (isSuccessful) {
                Toast.makeText(this, "Payment Success!", Toast.LENGTH_LONG).show()
                preferencesManager.put(PreferenceManager.Key.IS_APP_ADS_FREE, true)
            } else {
                Toast.makeText(this, "Operation failed please try again", Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun checkSubscribedProducts() {
        val subscriptionList: List<String> = billingManager.subscriptionList
        if (subscriptionList.isNotEmpty()) {
            preferencesManager.put(PreferenceManager.Key.IS_APP_ADS_FREE, true)
        } else {
            preferencesManager.put(PreferenceManager.Key.IS_APP_ADS_FREE, false)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (billingManager.onActivityResult(requestCode, resultCode, data))
            super.onActivityResult(requestCode, resultCode, data)
    }

    private fun registerBroadCastReceiver() {

        broadCastListener = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                val daily = intent.getBooleanExtra(Constants.SCREEENTIME_DAILY, false)
                val monthly = intent.getBooleanExtra(Constants.SCREEENTIME_MONTHLY, false)
                val weekly = intent.getBooleanExtra(Constants.SCREEENTIME_WEEKLY, false)
                val screenTime = intent.getLongExtra(Constants.SCREEENTIME_DRAWER, 0)
                if (daily) {
                    headerBinding.drawerLayout.visibility = View.GONE
                    setDrawerScreenTime("daily", timeFormat(screenTime))
                } else if (monthly) {
                    headerBinding.drawerLayout.visibility = View.GONE
                    setDrawerScreenTime("monthly", timeFormat(screenTime))
                } else if (weekly) {
                    headerBinding.drawerLayout.visibility = View.GONE
                    setDrawerScreenTime("weekly", timeFormat(screenTime))
                }

            }
        }
        LocalBroadcastManager.getInstance(this).registerReceiver(broadCastListener, IntentFilter(Constants.DRAWER_UPDATE_EVENT))
    }


    private fun setDrawerScreenTime(interval: String, screenTime: String) {
        headerBinding.drawerLayout.visibility = View.VISIBLE
        if (interval.equals("daily")) {
            headerBinding.dailyScreenTimeLayout.visibility = View.VISIBLE
//           headerBinding.monthlyScreenTimeLayout.visibility = View.GONE
//           headerBinding.weeklyScreenTimeLayout.visibility = View.GONE

            headerBinding.dailyScreenTimeText.text = screenTime
        } else if (interval.equals("weekly")) {
//            headerBinding.dailyScreenTimeLayout.visibility = View.GONE
//            headerBinding.monthlyScreenTimeLayout.visibility = View.GONE
            headerBinding.weeklyScreenTimeLayout.visibility = View.VISIBLE

            headerBinding.weeklyScreenTimeText.text = screenTime

        } else if (interval.equals("monthly")) {
//            headerBinding.dailyScreenTimeLayout.visibility = View.GONE
            headerBinding.monthlyScreenTimeLayout.visibility = View.VISIBLE
//            headerBinding.weeklyScreenTimeLayout.visibility = View.GONE

            headerBinding.monthlyScreenTimeText.text = screenTime
        }
    }

    fun timeFormat(totalTimeInForeground: Long): String {
        val timeSec: Long = totalTimeInForeground / 1000
        var requiredFormat = "0 hr"
        val hours = timeSec.toInt() / 3600
        var temp = timeSec.toInt() - hours * 3600
        val mins = temp / 60
        temp = temp - mins * 60
        val secs = temp
        if (mins > 0) {
            if (hours > 0) {
                requiredFormat = "$hours hr, $mins min" //hh:mm:ss formatted string
            } else {
                requiredFormat = "$mins min" //hh:mm:ss formatted string
            }
        } else if (hours > 0) {
            requiredFormat = "$hours hr" //hh:mm:ss formatted string
        } else {
            requiredFormat = "$secs secs"
        }
        return requiredFormat
//        Log.d(TAG, "time: $requiredFormat")
    }

    override fun onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadCastListener)
        super.onDestroy()
    }
}