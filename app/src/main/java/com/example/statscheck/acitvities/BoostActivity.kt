package com.example.statscheck.acitvities

import android.animation.Animator
import android.app.usage.UsageStats
import android.app.usage.UsageStatsManager
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.ArrayMap
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.MutableLiveData
import com.example.statscheck.Constants
import com.example.statscheck.R
import com.example.statscheck.adapters.StatsAdapter
import com.example.statscheck.databinding.ActivityBoostBinding
import com.example.statscheck.helpers.AppNameComparator
import com.example.statscheck.helpers.LastTimeUsedComparator
import com.example.statscheck.helpers.UsageTimeComparator
import com.example.statscheck.managers.AdsManager
import com.example.statscheck.managers.PreferenceManager
import com.iobits.identification.managers.MoPubAdsManager
import com.ram.speed.booster.RAMBooster
import com.ram.speed.booster.interfaces.ScanListener
import com.ram.speed.booster.utils.ProcessInfo
import kotlinx.android.synthetic.main.booster_page_three.*
import kotlinx.android.synthetic.main.booster_page_three.view.*
import kotlinx.android.synthetic.main.booster_page_two.view.*
import kotlinx.android.synthetic.main.booster_page_two.view.successBoosterAnimation
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

class BoostActivity : BaseActivity() {
    @Inject
    lateinit var preferencesManager: PreferenceManager
    @Inject
    lateinit var moPubAdsManager: MoPubAdsManager
    private var mUsageStatsManager: UsageStatsManager? = null
    private lateinit var mInflater: LayoutInflater
    private lateinit var mAdapter: StatsAdapter
    private lateinit var mPm: PackageManager
    private val mPackageStats = ArrayList<UsageStats>()
    private val mAppLabelMap = ArrayMap<String, String>()
    private val mUsageTimeComparator = UsageTimeComparator()
    private lateinit var mAppLabelComparator: AppNameComparator
    private val mLastTimeUsedComparator = LastTimeUsedComparator()
    private val ramIsCleaned = MutableLiveData<Boolean>()
    private var total_screen_time: Long = 0
    private val TAG = "BoostActivity"
    private lateinit var binding: ActivityBoostBinding
    val booster = RAMBooster(this)
    private var mTotalRam =0L
    private var mAvailableRam =0L
    @Inject
    lateinit var adsManager: AdsManager
    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        binding = ActivityBoostBinding.inflate(layoutInflater)
        setContentView(binding.root)
//        setContentView(R.layout.activity_boost)

        init()
        loadAd()
        initResultBooster()
        startBoosting()

        ramIsCleaned.observe(this, { ready ->
            if (ready) {
                binding.ramBoosterTwoLayout.successBoosterAnimation.playAnimation()
                binding.ramBoosterPageOne.flipText.text = "Ram Is Cleaned"
                binding.ramBoosterOneLayout.visibility = View.GONE
                binding.ramBoosterTwoLayout.visibility = View.VISIBLE
//                binding.ramBoosterTwoLayout.successBoosterAnimation.addAnimatorListener(Animation)
                ramIsCleaned.value = false
            }
        })
    }

    private fun startBoosting() {
        CoroutineScope(Main).launch{
            getalldata()
            cleanRam()
            ramIsCleaned.value = true
        }
    }

    private fun initResultBooster() {
        binding.ramBoosterThreeLayout.visibility = View.GONE

//        moPubAdsManager.onSDKintialized(object : MoPubAdsManager.ISDKinitialize {
//            override fun onIntialized(isInit: Boolean) {
//                binding.ramBoosterPageOne.adFrame.removeAllViews()
//                moPubAdsManager.loadNativeAds(binding.ramBoosterThreeLayout.adFrame, Constants.MOPUB_NATIVE, MoPubAdsManager.NativeAdType.SMALL_TYPE)
//
//            }
//        })
//        if (moPubAdsManager.isInitialized) {
//            binding.ramBoosterPageOne.adFrame.removeAllViews()
//            moPubAdsManager.loadNativeAds(binding.ramBoosterThreeLayout.adFrame, Constants.MOPUB_NATIVE, MoPubAdsManager.NativeAdType.SMALL_TYPE)
//
//        }
        adsManager.loadNativeAd(this@BoostActivity, binding.ramBoosterPageOne.adFrame, AdsManager.NativeAdType.SMALL_TYPE)

        binding.ramBoosterThreeLayout.boostAgain.setOnClickListener {
            boostAgain()
        }

    }

    private fun boostAgain() {
        binding.ramBoosterOneLayout.visibility = View.VISIBLE
        binding.ramBoosterTwoLayout.visibility = View.GONE
        binding.ramBoosterThreeLayout.visibility = View.GONE
        CoroutineScope(Main).launch {
            cleanRam()
            ramIsCleaned.value = true
        }
    }

    private fun init() {
        binding.toolbar.toolbarTitleTextView.text = "Phone Booster"
        binding.toolbar.backArrow.setOnClickListener {
            onBackPressed()
        }
        mUsageStatsManager = getSystemService(AppCompatActivity.USAGE_STATS_SERVICE) as UsageStatsManager
        mInflater = getSystemService(AppCompatActivity.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        mPm = packageManager

        binding.ramBoosterTwoLayout.successBoosterAnimation.addAnimatorListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animation: Animator?) {
                Log.e("Animation:", "start")
            }

            override fun onAnimationEnd(animation: Animator?) {
                Log.e("Animation:", "end")
                binding.ramBoosterOneLayout.visibility = View.GONE
                binding.ramBoosterTwoLayout.visibility = View.GONE
                binding.ramBoosterThreeLayout.visibility = View.VISIBLE

                Log.d("onAnimationEnd", "onAnimationEnd: $mTotalRam")
                Log.d("onAnimationEnd", "onAnimationEnd: $mAvailableRam")
                binding.ramBoosterThreeLayout.freeRamAfterCleanText.text = ((mAvailableRam + 150).toString() + " Mb")

            }

            override fun onAnimationCancel(animation: Animator?) {
                Log.e("Animation:", "cancel")
            }

            override fun onAnimationRepeat(animation: Animator?) {
                Log.e("Animation:", "repeat")
            }
        })


        //getAvailble or total Ram
        booster.setScanListener(object : ScanListener {
            override fun onStarted() {
                Log.d(TAG, "onStarted: started")

            }

            override fun onFinished(availableRam: Long, totalRam: Long, appsToClean: List<ProcessInfo>) {
                Log.d(TAG, "onStarted: onFinished")
                Log.d(TAG, "onStarted: availableRam $availableRam")
                Log.d(TAG, "onStarted: totalRam $totalRam")
                Log.d(TAG, "onStarted: appsToClean ${appsToClean.size}")

                mAvailableRam = availableRam
                mTotalRam = totalRam
            }
        })

    }

    private suspend fun cleanRam() {
        booster.startScan(true)
        //            var i =mPackageStats.size
        var label = mAppLabelMap.get(mPackageStats.get(0).packageName)
        for (i in 0 until mPackageStats.size){
            label = mAppLabelMap.get(mPackageStats.get(i).packageName)
            binding.ramBoosterPageOne.flipText.text = label
            delay(100)
        }

//            while(i>=0){
//                label = mAppLabelMap.get(mPackageStats.get(0).packageName)
//                binding.flipText.text = label
//                --i
//                delay(5000)
//            }
    }

    private fun loadAd() {

//        moPubAdsManager.onSDKintialized(object : MoPubAdsManager.ISDKinitialize {
//            override fun onIntialized(isInit: Boolean) {
//                binding.ramBoosterPageOne.adFrame.removeAllViews()
//                moPubAdsManager.loadNativeAds(binding.ramBoosterPageOne.adFrame, Constants.MOPUB_NATIVE, MoPubAdsManager.NativeAdType.MEDIUM_TYPE)
//
//            }
//        })
//        if (moPubAdsManager.isInitialized) {
//            binding.ramBoosterPageOne.adFrame.removeAllViews()
//            moPubAdsManager.loadNativeAds(binding.ramBoosterPageOne.adFrame, Constants.MOPUB_NATIVE, MoPubAdsManager.NativeAdType.MEDIUM_TYPE)
//
//        }
        adsManager.loadNativeAd(this@BoostActivity, binding.ramBoosterPageOne.adFrame, AdsManager.NativeAdType.MEDIUM_TYPE)
        binding.ramBoosterPageOne.adFramCard.visibility = View.VISIBLE
    }

    fun getalldata() {
        val cal = Calendar.getInstance()
        cal.add(Calendar.DAY_OF_YEAR, -1)//-5 by default
        val stats = mUsageStatsManager?.queryUsageStats(UsageStatsManager.INTERVAL_MONTHLY,
                cal.timeInMillis, System.currentTimeMillis())
//        mUsageStatsManager?.queryAndAggregateUsageStats()

        val map = ArrayMap<String, UsageStats>()
        val statCount = stats!!.size

        for (i in 0 until statCount) {
            val pkgStats = stats[i]

            // load application labels for each application
            try {
                val appInfo = mPm.getApplicationInfo(pkgStats.packageName, 0)
                val label = appInfo.loadLabel(mPm).toString()
                if (!mAppLabelMap.contains(pkgStats.packageName)) {
                    mAppLabelMap[pkgStats.packageName] = label
                } else {
                    break
                }

                val existingStats = map[pkgStats.packageName]
                if (existingStats == null) {
                    val appUseTime = pkgStats.totalTimeInForeground / 1000
                    val neverUsedTime = 0

                    if (appUseTime > neverUsedTime) { //condition for add only those who used only
                        //adding pakage into map
                        total_screen_time = total_screen_time.plus(pkgStats.totalTimeInForeground)
//                        total_screen_time= total_screen_time.plus(pkgStats.totalTimeInForeground / 1000)
                        map[pkgStats.packageName] = pkgStats
                    }
                } else {
                    existingStats.add(pkgStats)
                }
            } catch (e: PackageManager.NameNotFoundException) {
                // This package may be gone.
            }
        }
//        mPackageStats.clear()
        mPackageStats.addAll(map.values)
        // Sort list
        mAppLabelComparator = AppNameComparator(mAppLabelMap)

    }
}