package com.example.statscheck.acitvities

import android.app.usage.UsageStats
import android.app.usage.UsageStatsManager
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.toBitmap
import androidx.palette.graphics.Palette
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.statscheck.Constants
import com.example.statscheck.R
import com.example.statscheck.adapters.DetailsAdapter
import com.example.statscheck.databinding.ActivityDetailScreenTimeBinding
import com.example.statscheck.helpers.LastTimeUsedComparator
import com.example.statscheck.managers.AdsManager
import com.example.statscheck.managers.PreferenceManager
import com.ghanshyam.graphlibs.Graph
import com.ghanshyam.graphlibs.GraphData
import com.iobits.identification.managers.MoPubAdsManager
import kotlinx.coroutines.*
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

class DetailScreenTimeActivity : BaseActivity() {
    private lateinit var mAdapter: DetailsAdapter
    private val TAG = "DetailScreenTimeActivit"
    private lateinit var mPackageStats: ArrayList<UsageStats>
    private var mAppStats: ArrayList<UsageStats> = ArrayList<UsageStats>()
    private var mPm: PackageManager? = null
    private lateinit var binding: ActivityDetailScreenTimeBinding
    private var AppLabel: String = ""
    private var mPosition: Int = 0
    private var mUsageStatsManager: UsageStatsManager? = null

    @Inject
    lateinit var preferencesManager: PreferenceManager
    @Inject
    lateinit var adsManager: AdsManager

    @Inject
    lateinit var moPubAdsManager: MoPubAdsManager
    private val mLastTimeUsedComparator = LastTimeUsedComparator()

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        binding = ActivityDetailScreenTimeBinding.inflate(layoutInflater)
        setContentView(binding.root)
//        setContentView(R.layout.activity_detail_screen_time)

        Log.d(TAG, "onCreate: activity onCreate")
        
        loadAd()
        //getintent data
        mPackageStats = intent.getParcelableArrayListExtra<UsageStats>("StatUsageList") as ArrayList<UsageStats>
        mPosition = intent.getIntExtra("position", 0)
        AppLabel = intent.getStringExtra("AppLabel").toString()
        val Percentage = intent.getStringExtra("Percentage")

        init()
        val vibrantSwatch = mPm?.getApplicationIcon(mPackageStats.get(mPosition).packageName)?.let { createPaletteSync(it.toBitmap()).vibrantSwatch }
        initPieChart(vibrantSwatch, Percentage)

        //details history is working in on resume
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG, "onResume: activity on resume")
        CoroutineScope(Dispatchers.IO).launch {
            getWeekDays()
            withContext(Dispatchers.Main) {
                binding.progressCircular.visibility = View.GONE
                binding.recyclerViewLayout.visibility = View.VISIBLE
                mAdapter.setData(AppLabel, mAppStats,
                        mPm!!, mPosition)
            }
        }

    }

    private fun loadAd() {

        adsManager.loadNativeAd(this@DetailScreenTimeActivity, binding.adFrame, AdsManager.NativeAdType.MEDIUM_TYPE)
    }

    private fun initPieChart(vibrantSwatch: Palette.Swatch?, Percentage: String?) {
        Glide.with(this)
                .load(mPm?.getApplicationIcon(mPackageStats.get(mPosition).packageName))
                .into(binding.pieChartImg)

        val data: MutableCollection<GraphData> = java.util.ArrayList()
        val graph = findViewById<Graph>(R.id.graph)
        graph.setMinValue(0f)
        graph.setMaxValue(100f)
        graph.setDevideSize(0.5f)
        graph.setBackgroundShapeWidthInDp(5)
        graph.setForegroundShapeWidthInDp(7)
        graph.setShapeForegroundColor(ResourcesCompat.getColor(getResources(), R.color.text_color_light, null))
        graph.setShapeBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.browser_actions_bg_grey, null))

        if (vibrantSwatch != null) {
            if (Percentage != null) {
                data.add(GraphData(Percentage.toFloat(), vibrantSwatch.rgb
                        ?: ContextCompat.getColor(this, R.color.default_badge_background_color)))
            } else {
                data.add(GraphData(65F, vibrantSwatch.rgb
                        ?: ContextCompat.getColor(this, R.color.default_badge_background_color)))
            }
        }
        graph.setData(data)
    }


    // Generate palette synchronously and return it
    fun createPaletteSync(bitmap: Bitmap): Palette = Palette.from(bitmap).generate()

    fun getCurrentDate(): String? {
        val cal = Calendar.getInstance()
        val sdf = SimpleDateFormat("EEE, d MMM, yyyy")
        return sdf.format(cal.time)
    }

    private fun init() {

        binding.toolbar.toolbarTitleTextView.text = AppLabel

        val pkgStats = mPackageStats.get(mPosition)
        mAppStats.add(pkgStats)
        binding.titleTxt.text = AppLabel
        binding.totalScreentime.text = setTimeInFormat(pkgStats.totalTimeInForeground)
        binding.currentDayDate.text = getCurrentDate()

        mUsageStatsManager = getSystemService(AppCompatActivity.USAGE_STATS_SERVICE) as UsageStatsManager
        mPm = packageManager

        binding.toolbar.backArrow.setOnClickListener {
            onBackPressed()
        }
        //set adapter in mopup adapter
        mAdapter = DetailsAdapter()
        val moPubAdapter = moPubAdsManager.getMoPubAdapter(this@DetailScreenTimeActivity, mAdapter, MoPubAdsManager.NativeAdType.BANNER_ADAPTER_TYPE)

        binding.detailStatRecyclerView.apply {
            this.layoutManager = LinearLayoutManager(this@DetailScreenTimeActivity)
            this.adapter = moPubAdapter
        }
        moPubAdapter.loadAds(Constants.MOPUB_NATIVE)
    }

    override fun onBackPressed() {
        adsManager.showInterstitialAd(this)
        super.onBackPressed()
    }


    fun setTimeInFormat(totalTimeInForeground: Long): String {
        val timeSec: Long = totalTimeInForeground / 1000
        var requiredFormat = "0 sec"
        val hours = timeSec.toInt() / 3600
        var temp = timeSec.toInt() - hours * 3600
        val mins = temp / 60
        temp = temp - mins * 60
        val secs = temp
        if (secs > 0) {
            if (mins > 0) {
                if (hours > 0) {
                    requiredFormat = "$hours hr, $mins min, $secs sec" //hh:mm:ss formatted string
                } else {
                    requiredFormat = "$mins min, $secs sec" //hh:mm:ss formatted string
                }
            } else {
                if (hours > 0) {
                    requiredFormat = "$hours hr, $mins min, $secs sec" //hh:mm:ss formatted string
                } else {
                    requiredFormat = "$secs sec" //hh:mm:ss formatted string
                }
            }
        } else if (hours > 0) {
            requiredFormat = "$hours hr, $mins min" //hh:mm:ss formatted string
        } else if (mins > 0) {
            requiredFormat = "$mins min" //hh:mm:ss formatted string
        }
        return requiredFormat
//        Log.d(TAG, "time: $requiredFormat")
    }

    fun getWeekDays() {
        val weekDayOne = Calendar.getInstance()
        val weekDayTwo = Calendar.getInstance()
        val weekDayThree = Calendar.getInstance()
        val weekDayFour = Calendar.getInstance()
        val weekDayFive = Calendar.getInstance()
        val weekDaySix = Calendar.getInstance()
        val weekDaySav = Calendar.getInstance()
        weekDayOne.add(Calendar.DAY_OF_YEAR, -7)//-5 by default
        weekDayTwo.add(Calendar.DAY_OF_YEAR, -6)//-5 by default
        weekDayThree.add(Calendar.DAY_OF_YEAR, -5)//-5 by default
        weekDayFour.add(Calendar.DAY_OF_YEAR, -4)//-5 by default
        weekDayFive.add(Calendar.DAY_OF_YEAR, -3)//-5 by default
        weekDaySix.add(Calendar.DAY_OF_YEAR, -2)//-5 by default
        weekDaySav.add(Calendar.DAY_OF_YEAR, -1)//-5 by default

        getScreenTodayScreenTime(weekDayFive.timeInMillis, weekDaySix.timeInMillis)
        getScreenTodayScreenTime(weekDayFour.timeInMillis, weekDayFive.timeInMillis)
        getScreenTodayScreenTime(weekDayThree.timeInMillis, weekDayFour.timeInMillis)
        getScreenTodayScreenTime(weekDayTwo.timeInMillis, weekDayThree.timeInMillis)
        getScreenTodayScreenTime(weekDayOne.timeInMillis, weekDayTwo.timeInMillis)

        Collections.sort(mAppStats, mLastTimeUsedComparator)

    }


    fun getScreenTodayScreenTime(beginTime: Long, timeInMillis: Long) {

        val stats = mUsageStatsManager?.queryUsageStats(UsageStatsManager.INTERVAL_DAILY,
                beginTime, timeInMillis)

        if (stats != null) {
            val statCount = stats.size

            for (i in 0 until statCount) {
                val pkgStats = stats[i]
                try {
                    val appInfo = mPm?.getApplicationInfo(pkgStats.packageName, 0)
                    val label = mPm?.let { appInfo?.loadLabel(it).toString() }

                    val appUseTime = pkgStats.totalTimeInForeground / 1000
                    val neverUsedTime = 0

                    if (appUseTime > neverUsedTime) {

                        if (label.equals(AppLabel)) {
                            var dublicated = false
                            for (o in 0 until mAppStats.size) {
                                dublicated = mAppStats.get(o).lastTimeUsed.equals(pkgStats.lastTimeUsed)
                                if (dublicated) {
                                    break
                                }
                            }
//                            val lastAdded = mAppStats.size - 1
//                            val dublicated = mAppStats.get(lastAdded).lastTimeUsed.equals(pkgStats.lastTimeUsed)

                            if (!dublicated) { // bcz of duplicated last value in arraylist
                                mAppStats.add(pkgStats)
                            }
                        }
                    }
                } catch (e: PackageManager.NameNotFoundException) {
                    // This package may be gone.
                }


            }
        }
    }
}