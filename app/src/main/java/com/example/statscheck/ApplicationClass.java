package com.example.statscheck;

import com.example.statscheck.managers.AdsManager;
import com.example.statscheck.managers.BillingManager;
import com.example.statscheck.managers.PreferenceManager;
import com.iobits.identification.di.AppComponent;

import com.iobits.identification.di.DaggerAppComponent;
import com.iobits.identification.managers.MoPubAdsManager;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;

public class ApplicationClass extends DaggerApplication {
    private static final String TAG = "ApplicationClass";

    @Inject
    public PreferenceManager preferenceManager;

    @Inject
    public MoPubAdsManager moPubAdsManager;

    @Inject
    public AdsManager adsManager;

    public static ApplicationClass mInstance;
    @Inject
    public BillingManager billingManager;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

//        DonationBillingManager.getInstance();
        moPubAdsManager.initializeMoPub();
//        Stetho.initializeWithDefaults(this);
    }

    public static ApplicationClass getInstance() {
        return mInstance;
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        AppComponent appComponent = DaggerAppComponent.builder().application(this).build();
        appComponent.inject(this);
        return appComponent;
    }


}
