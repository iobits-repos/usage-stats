

import android.app.Activity
import android.content.Context
import android.content.IntentSender.SendIntentException
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.example.statscheck.R
import com.google.android.material.snackbar.Snackbar
import java.text.SimpleDateFormat
import java.util.*

fun Context.makeToastShort(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}

fun Context.makeToastLong(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}

fun Context.logd(message: String, tag: String) {
    Log.d(tag, "logd: $tag : $message")
}

fun Activity.showSnackBar(string: String) {
    val parentLayout: View = findViewById(android.R.id.content)
    Snackbar.make(parentLayout, string, Snackbar.LENGTH_SHORT)
            .setBackgroundTint(ContextCompat.getColor(this, R.color.colorPrimaryDark))
            .setTextColor(ContextCompat.getColor(this, R.color.colorWhite))
            .show()
}



// Glide images
fun Activity.loadGlideNormal(url: String?, image: ImageView) {
    url?.let {it->
        try {
            Glide.with(this)
                    .load(it)
                    .into(image)
        }catch (e:GlideException){
            logd(e.localizedMessage!!,"GLIDE_REQUEST_KTX")
        }

    }

}



fun Context.loadImageBitmap(url: String): Bitmap? {
    return try {
        Glide.with(this).asBitmap()
                .load(url)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.DATA)
                .submit(256 //px
                        , 256 //px
                )
                .get()
    } catch (e: Exception) {
        null
    }
}

fun Context.loadImageBitmapBigSize(url: String): Bitmap? {
    return try {
        Glide.with(this).asBitmap()
                .load(url)
                .diskCacheStrategy(DiskCacheStrategy.DATA)
                .submit(720 //px
                        , 720 //px
                )
                .get()
    } catch (e: Exception) {
        null
    }
}

// DATE Time
fun Context.getFormattedDateTime(createdAt: Long): String {
    //check if time diff is greater then 24 hours then get complete date other wise get time
    return if (Date().time - createdAt > 86400000)
        getFormattedDate(createdAt)
    else
        getFormattedTime(createdAt)
}

fun Context.getFormattedTime(timeInMilliSec: Long): String {
    val sdf = SimpleDateFormat("hh:mm aaa", Locale.US)
    val date = Date(timeInMilliSec)
    return String.format(sdf.format(date))
}

fun Context.getFormattedDate(timeInMilliSec: Long): String {
    val sdf = SimpleDateFormat("MMM d, yyyy", Locale.US)
    val date = Date(timeInMilliSec)
    return String.format(sdf.format(date))
}


// Drawable helper methods

fun Context.getDrawableKtx(drawableId: Int): Drawable? {
   return ContextCompat.getDrawable(this,drawableId)
}