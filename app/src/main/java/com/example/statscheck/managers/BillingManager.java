package com.example.statscheck.managers;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Singleton;

import static com.example.statscheck.Constants.ITEM_SKU;
import static com.example.statscheck.Constants.MERCHANT_ID;
import static com.example.statscheck.Constants.PLAY_BILLING_LICENSE_KEY;

//import static com.iobits.identification.Constants.ITEM_SKU;
//import static com.iobits.identification.Constants.MERCHANT_ID;
//import static com.iobits.identification.Constants.PLAY_BILLING_LICENSE_KEY;


@Singleton
public class BillingManager implements PurchasesUpdatedListener {
//    adb shell pm clear com.android.vending

    BillingProcessor bp;
    private static final String LOG_TAG = "iabv3";

    private BillingClient billingClient;
    private boolean clientReady = false;

    private static BillingManager manager;
    private InAppPurchaseListener listener;

    public void setListener(InAppPurchaseListener listener) {
        this.listener = listener;
    }

    private Context context;

/*    public static BillingManager getInstance() {
        if (manager == null) {
            manager = new BillingManager();
        }
        return manager;
    }*/

    @Inject
    public BillingManager(Context context) {
        bp = new BillingProcessor(context, PLAY_BILLING_LICENSE_KEY, MERCHANT_ID, new BillingProcessor.IBillingHandler() {
            @Override
            public void onProductPurchased(@NonNull String productId, @Nullable TransactionDetails details) {
                if (listener != null) {
                    listener.isSuccessful(true);
                }
            }

            @Override
            public void onBillingError(int errorCode, @Nullable Throwable error) {
                if (listener != null) {
                    listener.isSuccessful(false);
                }
            }

            @Override
            public void onBillingInitialized() {
            }

            @Override
            public void onPurchaseHistoryRestored() {
                //showToast("onPurchaseHistoryRestored");
                for (String sku : bp.listOwnedProducts()) {
                    Log.d(LOG_TAG, "Owned Managed Product: " + sku);
                    if (sku.equals(ITEM_SKU)) {
                    }
                }
                for (String sku : bp.listOwnedSubscriptions())
                    Log.d(LOG_TAG, "Owned Subscription: " + sku);
                //updateTextViews();
            }
        });

        billingClient = BillingClient.newBuilder(context).setListener(this).enablePendingPurchases().build();
        billingClient.startConnection(new BillingClientStateListener() {

            @Override
            public void onBillingSetupFinished(BillingResult billingResult) {
                clientReady = true;
            }

            @Override
            public void onBillingServiceDisconnected() {
            }
        });
    }

    //Donation Function
    public void donateMoney(Activity activity, String ITEM_SKU,InAppPurchaseListener listener) {
        // NEW lib Code
        this.listener=listener;
        bp.purchase(activity, ITEM_SKU);
    }

    public void onDestroy() {
        if (bp != null) {
            bp.release();
        }
    }

    public void subscribe(Activity activity, String packageId,InAppPurchaseListener listener) {
        this.listener=listener;
        bp.subscribe(activity, packageId);
    }

    public List<String> getSubscriptionList() {
        return bp.listOwnedSubscriptions();

    }

    public boolean onActivityResult(int requestCode, int resultCode, Intent data) {
        boolean flag = false;
        if (!bp.handleActivityResult(requestCode, resultCode, data)) {
            flag = true;
        }
        return flag;
    }

    public com.anjlab.android.iab.v3.SkuDetails getProductPrice(String productId) {
        return bp.getSubscriptionListingDetails(productId);

    }

   public void getSubscriptionItemDetails(List<String> itemSkuList,SubscriptionDetailsListener listener) {
        if (clientReady) {
            SkuDetailsParams.Builder params = SkuDetailsParams.newBuilder();
            params.setSkusList(itemSkuList).setType(BillingClient.SkuType.SUBS);
            billingClient.querySkuDetailsAsync(params.build(),
                    (billingResult, skuDetailsList) -> {
                        if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK && skuDetailsList != null) {
                          listener.onDetailsLoad(skuDetailsList);
                        }
                    });
        }
    }

    @Override
    public void onPurchasesUpdated(BillingResult billingResult, @Nullable List<Purchase> purchases) {

    }

    public interface SubscriptionDetailsListener{
        void onDetailsLoad(List<SkuDetails> skuDetailsList);
    }

    public interface InAppPurchaseListener {
        void isSuccessful(boolean isPurchased);
    }

}