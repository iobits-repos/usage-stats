package com.iobits.identification.managers

import android.app.ActionBar
import android.app.Activity
import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.RelativeLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.statscheck.Constants
import com.example.statscheck.R
import com.example.statscheck.managers.PreferenceManager
import com.mopub.common.MoPub
import com.mopub.common.SdkConfiguration
import com.mopub.common.SdkInitializationListener
import com.mopub.mobileads.MoPubErrorCode
import com.mopub.mobileads.MoPubInterstitial
import com.mopub.mobileads.MoPubView
import com.mopub.mobileads.MoPubView.BannerAdListener
import com.mopub.nativeads.*
import com.mopub.nativeads.FacebookAdRenderer.FacebookViewBinder
import com.mopub.nativeads.MoPubNative.MoPubNativeNetworkListener
import com.mopub.nativeads.MoPubNativeAdPositioning.MoPubServerPositioning
import com.mopub.nativeads.NativeAd.MoPubNativeEventListener

import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MoPubAdsManager @Inject constructor(private val preferencesManager: PreferenceManager, private val context: Context) {
    private val TAG = MoPubAdsManager::class.java.name

    enum class NativeAdType {
        REGULAR_TYPE, BANNER_TYPE, MEDIUM_TYPE, SMALL_TYPE, SMALL_ADAPTER_TYPE, MEDIUM_ADAPTER_TYPE, BANNER_ADAPTER_TYPE
    }

    private var moPubView: MoPubView? = null
    private var moPubNative: MoPubNative? = null
    private var adapterHelper: AdapterHelper? = null
    private var moPubNativeEventListener: MoPubNativeEventListener? = null
    private var mInterstitial: MoPubInterstitial? = null
    private var isdKinitialize: ISDKinitialize? = null
    private val isEnableAds = true

    // initialization function is call to initialize SDK - call it splash Activity or Application class
    fun initializeMoPub() {
        if (!isNetworkAvailable(context) || !isEnableAds) return
        val extras = Bundle()
        if (MoPub.canCollectPersonalInformation()) {
            extras.putString("npa", "1")
            Log.d(TAG, "ConsentData is Personalized")
        } else {
            extras.putString("npa", "0")
            Log.d(TAG, "ConsentData is nonPersonalized")
        }
//        Networking.getRequestQueue(context)
        showConsentForm()
    }

    // destroyAdsView function is call to destroy Ads Views - call it in onDestroy function of each Activity
    fun destroyAdsView() {
        if (moPubView != null) {
            moPubView!!.destroy()
            moPubView = null
        }
        if (mInterstitial != null) {
            mInterstitial?.destroy()
            mInterstitial = null
        }
        if (moPubNative != null) {
            moPubNative?.destroy()
            moPubNative = null
            moPubNativeEventListener = null
        }
    }

    private fun initSdkListener(): SdkInitializationListener {
        return SdkInitializationListener { /* MoPub SDK initialized.
            Check if you should show the consent dialog here, and make your ad requests. */
            Log.d(TAG, "onInitializationFinished")
            isdKinitialize?.onIntialized(true)
        }
    }

    val isInitialized: Boolean
        get() = MoPub.isSdkInitialized() && isNetworkAvailable(context) && isEnableAds

    private fun showConsentForm() {
        if (!isNetworkAvailable(context) || !isEnableAds) return
        val sdkConfiguration = SdkConfiguration.Builder(Constants.MOPUB_INTERSTITIAL)
                .build()
        MoPub.initializeSdk(context, sdkConfiguration, initSdkListener())

    }

    private fun isNetworkAvailable(mContext: Context): Boolean {
        val connectivityManager = mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    fun showMoPubBanner(AdUnit_id_banner: String, moPubView1: View?) {
        if (!preferencesManager.getBoolean(PreferenceManager.Key.IS_APP_ADS_FREE, false)) {
            moPubView = moPubView1 as MoPubView?
            if (!isInitialized) return
            moPubView?.setAdUnitId(AdUnit_id_banner)
            moPubView?.setLocalExtras(localExtraMap())
            moPubView?.loadAd()
            moPubView?.bannerAdListener = object : BannerAdListener {
                override fun onBannerLoaded(banner: MoPubView) {
                    Log.e(TAG, "onBannerLoaded")
                    moPubView?.visibility = View.VISIBLE
                }

                override fun onBannerFailed(banner: MoPubView, errorCode: MoPubErrorCode) {
                    Log.e(TAG, "onBannerFailed")
                    when (errorCode) {
                        MoPubErrorCode.NO_FILL ->                             // ("No inventory.")
                            Log.e(TAG, "NO_FILL")
                        MoPubErrorCode.SERVER_ERROR ->                             //("Unable to connect to MoPub adserver.")
                            Log.e(TAG, "SERVER_ERROR")
                        MoPubErrorCode.ADAPTER_NOT_FOUND -> {
                        }
                        MoPubErrorCode.ADAPTER_CONFIGURATION_ERROR -> {
                        }
                        MoPubErrorCode.NETWORK_TIMEOUT ->                             //("Third-party network failed to respond in a timely manner."),
                            Log.e(TAG, "NETWORK_TIMEOUT")
                        MoPubErrorCode.NETWORK_INVALID_STATE -> {
                        }
                        MoPubErrorCode.MRAID_LOAD_ERROR -> {
                        }
                        MoPubErrorCode.CANCELLED -> {
                        }
                        MoPubErrorCode.UNSPECIFIED -> {
                        }
                    }
                }

                override fun onBannerClicked(banner: MoPubView) {
                    Log.e(TAG, "onBannerClicked")
                }

                override fun onBannerExpanded(banner: MoPubView) {
                    Log.e(TAG, "onBannerExpanded")
                }

                override fun onBannerCollapsed(banner: MoPubView) {
                    Log.e(TAG, "onBannerCollapsed")
                }
            }
        }
    }

    private fun localExtraMap(): Map<String, Any> {
        val localExtras: MutableMap<String, Any> = HashMap()
        localExtras["testDevices"] = "dc0b7751-92a9-4cbe-9c79-df43a374a69e"
        //localExtras.put("testDevices", "F7791C457D40D3516832A6E00C8EFF3C");
        //localExtras.put("ad_choices_placement", NativeAdOptions.ADCHOICES_TOP_LEFT);
        return localExtras
    }

    fun loadMoPubInterstitial(AdUnit_id: String, activity: Activity) {
        if (!preferencesManager.getBoolean(PreferenceManager.Key.IS_APP_ADS_FREE, false)) {
            if (!isInitialized) return
            if (mInterstitial != null) {
                mInterstitial?.destroy()
                mInterstitial = null
            }
            mInterstitial = MoPubInterstitial(activity, AdUnit_id)
            mInterstitial?.setLocalExtras(localExtraMap())
            mInterstitial?.load()
            mInterstitial?.interstitialAdListener = object : MoPubInterstitial.InterstitialAdListener {
                override fun onInterstitialLoaded(interstitial: MoPubInterstitial) {
                    Log.e(TAG, "onInterstitialLoaded")
                }

                override fun onInterstitialFailed(interstitial: MoPubInterstitial, errorCode: MoPubErrorCode) {
                    Log.e(TAG, "onInterstitialFailed")
                    when (errorCode) {
                        MoPubErrorCode.NO_FILL ->
                            // ("No inventory.")
                            Log.e(TAG, "NO_FILL")
                        MoPubErrorCode.SERVER_ERROR ->
                            //("Unable to connect to MoPub adserver.")
                            Log.e(TAG, "SERVER_ERROR")
                        MoPubErrorCode.ADAPTER_NOT_FOUND -> {
                        }
                        MoPubErrorCode.ADAPTER_CONFIGURATION_ERROR -> {
                        }
                        MoPubErrorCode.NETWORK_TIMEOUT ->
                            //("Third-party network failed to respond in a timely manner."),
                            Log.e(TAG, "NETWORK_TIMEOUT")
                        MoPubErrorCode.NETWORK_INVALID_STATE -> {
                        }
                        MoPubErrorCode.MRAID_LOAD_ERROR -> {
                        }
                        MoPubErrorCode.CANCELLED -> {
                        }
                        MoPubErrorCode.UNSPECIFIED -> {
                        }
                        else -> {
                        }
                    }
                }

                override fun onInterstitialShown(interstitial: MoPubInterstitial) {
                    Log.e(TAG, "onInterstitialShown")
                    loadMoPubInterstitial(Constants.MOPUB_INTERSTITIAL, activity)
                }

                override fun onInterstitialClicked(interstitial: MoPubInterstitial) {
                    Log.e(TAG, "onInterstitialClicked")
                }

                override fun onInterstitialDismissed(interstitial: MoPubInterstitial) {
                    Log.e(TAG, "onInterstitialDismissed")
                    loadMoPubInterstitial(AdUnit_id, activity)
                }
            }
        }
    }

    private fun waitnShowMoPubInterstital() {
        if (!isInitialized) return
        if (mInterstitial == null) {
            return
        }
        mInterstitial?.interstitialAdListener = object : MoPubInterstitial.InterstitialAdListener {
            override fun onInterstitialLoaded(interstitial: MoPubInterstitial) {
                Log.e(TAG, "onInterstitialLoaded")
                mInterstitial?.show()
            }

            override fun onInterstitialFailed(interstitial: MoPubInterstitial, errorCode: MoPubErrorCode) {
                Log.e(TAG, "onInterstitialFailed")
                when (errorCode) {
                    MoPubErrorCode.NO_FILL ->                         // ("No inventory.")
                        Log.e(TAG, "NO_FILL")
                    MoPubErrorCode.SERVER_ERROR ->                         //("Unable to connect to MoPub adserver.")
                        Log.e(TAG, "SERVER_ERROR")
                    MoPubErrorCode.ADAPTER_NOT_FOUND -> {
                    }
                    MoPubErrorCode.ADAPTER_CONFIGURATION_ERROR -> {
                    }
                    MoPubErrorCode.NETWORK_TIMEOUT ->                         //("Third-party network failed to respond in a timely manner."),
                        Log.e(TAG, "NETWORK_TIMEOUT")
                    MoPubErrorCode.NETWORK_INVALID_STATE -> {
                    }
                    MoPubErrorCode.MRAID_LOAD_ERROR -> {
                    }
                    MoPubErrorCode.CANCELLED -> {
                    }
                    MoPubErrorCode.UNSPECIFIED -> {
                    }
                }
            }

            override fun onInterstitialShown(interstitial: MoPubInterstitial) {
                Log.e(TAG, "onInterstitialShown")
            }

            override fun onInterstitialClicked(interstitial: MoPubInterstitial) {
                Log.e(TAG, "onInterstitialClicked")
            }

            override fun onInterstitialDismissed(interstitial: MoPubInterstitial) {
                Log.e(TAG, "onInterstitialDismissed")
            }
        }
    }

    // Defined by your application, indicating that you're ready to show an interstitial ad.
    fun showMoPubInterstitial() {
        if (!preferencesManager.getBoolean(PreferenceManager.Key.IS_APP_ADS_FREE, false)) {
            if (!isInitialized) return
            if (mInterstitial == null) return
            if (mInterstitial?.isReady == true) {
                mInterstitial?.show()
            } else {
                // Caching is likely already in progress if `isReady()` is false.
                // Avoid calling `load()` here and instead rely on the callbacks as suggested below.
                Log.e(TAG, "Interstitial waiting")
                waitnShowMoPubInterstital()
            }
        }
    }

    fun loadNativeAds(parentView: FrameLayout, AdUnit_id: String, nativeAdType: NativeAdType) {
        if (!isInitialized) {
            Log.d(TAG, "loadNativeAds: trying to show ad but SDK not initialized")
            return
        }
        val nativeAdView = RelativeLayout(context)
        adapterHelper = AdapterHelper(context, 0, 3) // When standalone, any range will be fine.
        moPubNativeEventListener = object : MoPubNativeEventListener {
            override fun onImpression(view: View?) {
                Log.d(TAG, "onImpression")
                // Impress is recorded - do what is needed AFTER the ad is visibly shown here.
            }

            override fun onClick(view: View?) {
                Log.d(TAG, "onClick")
                // Click tracking.
            }
        }
        val moPubNativeNetworkListener: MoPubNativeNetworkListener = object : MoPubNativeNetworkListener {
            override fun onNativeLoad(nativeAd: NativeAd) {
                Log.d(TAG, "onNativeLoad")

                // Retrieve the pre-built ad view that AdapterHelper prepared for us.
                val v = adapterHelper?.getAdView(null, nativeAdView, nativeAd, ViewBinder.Builder(0).build())

                // Set the native event listeners (onImpression, and onClick).
                nativeAd.setMoPubNativeEventListener(moPubNativeEventListener)
                v?.layoutParams = ActionBar.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)

                // Add the ad view to our view hierarchy, defined in the XML (activity_main.xml).
                parentView.addView(v)
            }

            override fun onNativeFail(errorCode: NativeErrorCode) {
                Log.d(TAG, "onNativeFail: $errorCode")
                when (errorCode) {
                    NativeErrorCode.NETWORK_TIMEOUT ->
                        //("Third-party network failed to respond in a timely manner."),
                        Log.e(TAG, "NETWORK_TIMEOUT")
                    NativeErrorCode.NETWORK_INVALID_STATE -> {
                    }
                    NativeErrorCode.UNSPECIFIED -> {
                    }
                    else -> {
                    }
                }
            }
        }
        moPubNative = MoPubNative(context, AdUnit_id, moPubNativeNetworkListener)
        moPubNative?.setLocalExtras(localExtraMap())
        var faceBookLayout = 0
        var adMobLayout = 0
        var moPubLayout = 0
        when (nativeAdType) {
            NativeAdType.SMALL_TYPE -> {
                moPubLayout = R.layout.ad_app_install_mopub_small
                faceBookLayout = R.layout.ad_app_install_facebook_for_mopub_small
                adMobLayout = R.layout.ad_app_install_admob_for_mopub_small
            }
            NativeAdType.BANNER_TYPE -> {
                moPubLayout = R.layout.ad_app_install_mopub_banner
                faceBookLayout = R.layout.ad_app_install_facebook_for_mopub_banner
                adMobLayout = R.layout.ad_app_install_admob_for_mopub_banner
            }
            NativeAdType.MEDIUM_TYPE -> {
                moPubLayout = R.layout.ad_app_install_mopub_medium
                faceBookLayout = R.layout.ad_app_install_facebook_for_mopub_medium
                adMobLayout = R.layout.ad_app_install_admob_for_mopub_medium
            }
            NativeAdType.REGULAR_TYPE -> {
                moPubLayout = R.layout.ad_app_install_mopub_regular
                faceBookLayout = R.layout.ad_app_install_facebook_for_mopub_regular
                adMobLayout = R.layout.ad_app_install_admob_for_mopub_regular
            }
        }
        val moPubStaticNativeAdRenderer = MoPubStaticNativeAdRenderer(
                ViewBinder.Builder(moPubLayout)
                        .titleId(R.id.native_title)
                        .textId(R.id.native_text)
                        .mainImageId(R.id.native_main_image)
                        .iconImageId(R.id.native_icon_image)
                        .callToActionId(R.id.native_cta)
                        .privacyInformationIconImageId(R.id.native_privacy_information_icon_image)
                        .build()
        )
        val googlePlayServicesAdRenderer = GooglePlayServicesAdRenderer(
                GooglePlayServicesViewBinder.Builder(adMobLayout)
                        .titleId(R.id.native_title)
                        .textId(R.id.native_text)
                        .mediaLayoutId(R.id.native_main_image)
                        .iconImageId(R.id.native_icon_image)
                        .callToActionId(R.id.native_cta) // .addExtra("key_star_rating",R.id.appinstall_stars)
                        .privacyInformationIconImageId(R.id.native_privacy_information_icon_image)
                        .build())
        val facebookAdRenderer = FacebookAdRenderer(
                FacebookViewBinder.Builder(faceBookLayout)
                        .titleId(R.id.native_title)
                        .textId(R.id.native_text) // Binding to new layouts from Facebook 4.99.0+
                        .mediaViewId(R.id.native_main_image)
                        .adIconViewId(R.id.native_icon_image)
                        .adChoicesRelativeLayoutId(R.id.native_privacy_information_icon_image)
                        .advertiserNameId(R.id.native_title) // Bind either the titleId or advertiserNameId depending on the FB SDK version
                        // End of binding to new layouts
                        .callToActionId(R.id.native_cta)
                        .build())
        moPubNative?.registerAdRenderer(moPubStaticNativeAdRenderer)
        moPubNative?.registerAdRenderer(googlePlayServicesAdRenderer)
        moPubNative?.registerAdRenderer(facebookAdRenderer)

        // Send out the ad request.
        if (!preferencesManager.getBoolean(PreferenceManager.Key.IS_APP_ADS_FREE, false))
            moPubNative?.makeRequest()
    }

    fun load_CP_ICON_Ads(parentView: FrameLayout, AdUnit_id: String) {
        if (!isInitialized) return
        val nativeAdView = RelativeLayout(context)
        adapterHelper = AdapterHelper(context, 0, 3) // When standalone, any range will be fine.
        moPubNativeEventListener = object : MoPubNativeEventListener {
            override fun onImpression(view: View) {
                Log.d(TAG, "onImpression")
                // Impress is recorded - do what is needed AFTER the ad is visibly shown here.
            }

            override fun onClick(view: View) {
                Log.d(TAG, "onClick")
                // Click tracking.
            }
        }
        val moPubNativeNetworkListener: MoPubNativeNetworkListener = object : MoPubNativeNetworkListener {
            override fun onNativeLoad(nativeAd: NativeAd) {
                Log.d(TAG, "onNativeLoad")
                val v = adapterHelper?.getAdView(null, nativeAdView, nativeAd, ViewBinder.Builder(0).build())

                // Set the native event listeners (onImpression, and onClick).
                nativeAd.setMoPubNativeEventListener(moPubNativeEventListener)
                v?.layoutParams = ActionBar.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)

                // Add the ad view to our view hierarchy, defined in the XML (activity_main.xml).
                parentView.addView(v)
            }

            override fun onNativeFail(errorCode: NativeErrorCode) {
                Log.d(TAG, "onNativeFail: $errorCode")
            }
        }
        moPubNative = MoPubNative(context, AdUnit_id, moPubNativeNetworkListener)
        val moPubStaticNativeAdRenderer = MoPubStaticNativeAdRenderer(
                ViewBinder.Builder(R.layout.ad_cp_icon)
                        .iconImageId(R.id.native_icon_image)
                        .build()
        )
        moPubNative?.registerAdRenderer(moPubStaticNativeAdRenderer)
        // Send out the ad request.
        if (!preferencesManager.getBoolean(PreferenceManager.Key.IS_APP_ADS_FREE, false))
        moPubNative?.makeRequest()
    }

    fun getMoPubAdapter(activity: Activity, adapter: Any, nativeAdType: NativeAdType): MoPubRecyclerAdapter {

        var faceBookLayout = 0
        var adMobLayout = 0
        var moPubLayout = 0

        when (nativeAdType) {
            NativeAdType.MEDIUM_ADAPTER_TYPE -> {
                moPubLayout = R.layout.ad_app_install_mopub_medium
                faceBookLayout = R.layout.ad_app_install_facebook_for_mopub_medium
                adMobLayout = R.layout.ad_app_install_admob_for_mopub_medium
            }
            NativeAdType.SMALL_ADAPTER_TYPE -> {
                moPubLayout = R.layout.ad_app_install_mopub_small_adapter
                faceBookLayout = R.layout.ad_app_install_facebook_for_mopub_small_adapter
                adMobLayout = R.layout.ad_app_install_admob_for_mopub_small_adapter
            }
            NativeAdType.BANNER_ADAPTER_TYPE -> {
                moPubLayout = R.layout.ad_app_install_mopub_banner
                faceBookLayout = R.layout.ad_app_install_facebook_for_mopub_banner
                adMobLayout = R.layout.ad_app_install_admob_for_mopub_banner
            }
            else -> null
        }

        val myViewBinder = ViewBinder.Builder(moPubLayout)
                .titleId(R.id.native_title)
                .textId(R.id.native_text)
                .iconImageId(R.id.native_icon_image)
                .mainImageId(R.id.native_main_image)
                .callToActionId(R.id.native_cta)
                .privacyInformationIconImageId(R.id.native_privacy_information_icon_image)
                .build()
        val myRenderer = MoPubStaticNativeAdRenderer(myViewBinder)
        val googlePlayServicesAdRenderer = GooglePlayServicesAdRenderer(
                GooglePlayServicesViewBinder.Builder(adMobLayout)
                        .titleId(R.id.native_title)
                        .textId(R.id.native_text)
                        .iconImageId(R.id.native_icon_image)
                        .mediaLayoutId(R.id.native_main_image)
                        .callToActionId(R.id.native_cta)
                        .privacyInformationIconImageId(R.id.native_privacy_information_icon_image)
                        .build())
        val facebookAdRenderer = FacebookAdRenderer(
                FacebookViewBinder.Builder(faceBookLayout)
                        .titleId(R.id.native_title)
                        .textId(R.id.native_text) // Binding to new layouts from Facebook 4.99.0+
                        .mediaViewId(R.id.native_main_image)
                        .adIconViewId(R.id.native_icon_image)
                        .adChoicesRelativeLayoutId(R.id.native_privacy_information_icon_image)
                        .advertiserNameId(R.id.native_title) // Bind either the titleId or advertiserNameId depending on the FB SDK version
                        // End of binding to new layouts
                        .callToActionId(R.id.native_cta)
                        .build())

        val moPubAdAdapter = MoPubRecyclerAdapter(activity, (adapter as RecyclerView.Adapter<*>), MoPubServerPositioning())
        moPubAdAdapter.registerAdRenderer(myRenderer)
        moPubAdAdapter.registerAdRenderer(googlePlayServicesAdRenderer)
        moPubAdAdapter.registerAdRenderer(facebookAdRenderer)
        return moPubAdAdapter
    }

    fun onSDKintialized(isdKinitialize: ISDKinitialize?) {
        this.isdKinitialize = isdKinitialize
    }

    interface ISDKinitialize {
        fun onIntialized(isInit: Boolean)
    }


}