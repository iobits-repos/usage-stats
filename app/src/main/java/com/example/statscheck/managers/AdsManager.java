package com.example.statscheck.managers;

/*
 * Created by tanv33r on 16/2/21.
 */

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.statscheck.BuildConfig;
import com.example.statscheck.Constants;
import com.example.statscheck.R;
import com.facebook.ads.Ad;
import com.facebook.ads.AdSettings;
import com.facebook.ads.AudienceNetworkAds;
import com.facebook.ads.InterstitialAdListener;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.RequestConfiguration;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.VideoOptions;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.google.android.gms.ads.nativead.NativeAd;
import com.google.android.gms.ads.nativead.NativeAdOptions;
import com.google.android.gms.ads.nativead.NativeAdView;
import com.google.android.gms.ads.rewarded.RewardedAd;

import java.util.Arrays;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class AdsManager {

    public enum NativeAdType {
        REGULAR_TYPE,
        MEDIUM_TYPE,
        SMALL_TYPE,
        NOMEDIA_MEDIUM
    }

    private static AdsManager manager;
    private RewardedAd rewardedVideoAd;
    private com.facebook.ads.InterstitialAd fbInterstitialAd;
    private InterstitialAd admobInterstitialAd;
    private final String TAG = AdsManager.class.getName();
    private final PreferenceManager preferenceManager;


    @Inject
    public AdsManager(Context context, PreferenceManager preferenceManager) {

        // Initialize the Mobile Ads SDK.
        MobileAds.initialize(context, initializationStatus -> {
            Log.d(TAG, "AdsManager: initializes");
        });
        MobileAds.setRequestConfiguration(
                new RequestConfiguration.Builder().setTestDeviceIds(Arrays.asList("F58A121CA6B48BE487E841B65137F635",
                        "F489CD7B6E9F529144AEEBD5D32D3417","47BD8D71811EF0D2F25B46868A922D56"))
                        .build());

        if (!AudienceNetworkAds.isInitialized(context)) {

            if (BuildConfig.DEBUG) {
                AdSettings.turnOnSDKDebugger(context);
            }

            AudienceNetworkAds
                    .buildInitSettings(context)
                    .withInitListener(initResult -> {
                        Log.d(TAG, "AdsManager: AudienceNetworkAds initialized successfully");
                        AdSettings.addTestDevice("cc2cb783-da81-49d7-99a6-cf48fc68a5b7");
                    })
                    .initialize();
        }
        this.preferenceManager = preferenceManager;
    }

 /*   public static AdsManager getInstance(Context context) {
        if (manager == null) {
            manager = new AdsManager(context);
        }
        return manager;
    }*/

    private AdRequest prepareAdRequest() {
        AdRequest adRequest;
        adRequest = new AdRequest.Builder().build();
        return adRequest;
    }

    public void showBanner(Activity activity, FrameLayout adContainerView) {
        if (!preferenceManager.getBoolean(PreferenceManager.Key.IS_APP_ADS_FREE, false)) {
            AdView adView = new AdView(activity);
            adView.setAdUnitId(Constants.ACTION_TYPE);
            adContainerView.addView(adView);

            AdSize adSize = getAdSize(activity);
            adView.setAdSize(adSize);

            adView.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    Log.d(TAG, "onAdLoaded: ");
                }

                @Override
                public void onAdFailedToLoad(LoadAdError loadAdError) {
                    super.onAdFailedToLoad(loadAdError);
                    Log.d(TAG, "onAdFailedToLoad: " + loadAdError.toString());
                }
            });

            if (isNetWorkAvailable(activity))
                adView.loadAd(prepareAdRequest());
        }
    }
    public void showCustomBanner(Activity activity, FrameLayout adContainerView,String size) {
        if (!preferenceManager.getBoolean(PreferenceManager.Key.IS_APP_ADS_FREE, false)) {
            AdView adView = new AdView(activity);
            adView.setAdUnitId(Constants.ADMOD_BANNER);
            adContainerView.addView(adView);

            AdSize adSize = getAdSize(activity);

            if(size.equals("LARGE_BANNER")){
                adView.setAdSize(AdSize.LARGE_BANNER);
            }else if(size.equals("MEDIUM_RECTANGLE")){
                adView.setAdSize(AdSize.MEDIUM_RECTANGLE);
            }else {
                adView.setAdSize(adSize);
            }


            adView.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    Log.d(TAG, "onAdLoaded: ");
                }

                @Override
                public void onAdFailedToLoad(LoadAdError loadAdError) {
                    super.onAdFailedToLoad(loadAdError);
                    Log.d(TAG, "onAdFailedToLoad: " + loadAdError.toString());
                }
            });

            if (isNetWorkAvailable(activity))
                adView.loadAd(prepareAdRequest());
        }
    }
    public void loadInterstitialAd(Activity activity) {
        if (!preferenceManager.getBoolean(PreferenceManager.Key.IS_APP_ADS_FREE, false)) {
            if (isNetWorkAvailable(activity)) {
                InterstitialAdLoadCallback interstitialAdLoadCallback = new InterstitialAdLoadCallback() {
                    @Override
                    public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                        super.onAdLoaded(interstitialAd);
                        Log.d(TAG, "onLoad: admob interstitial");
                        admobInterstitialAd = interstitialAd;
                    }

                    @Override
                    public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                        super.onAdFailedToLoad(loadAdError);
                        Log.d(TAG, "onAdFailedToLoad: admob interstitial. Loading facebook ad");
                        loadFacebookInterstitialAd(activity);
                    }
                };
                InterstitialAd.load(activity, Constants.ADMOD_INTERSTITIAL, prepareAdRequest(), interstitialAdLoadCallback);
            }
        }
    }

    public void showInterstitialAd(Activity activity) {
        if (!preferenceManager.getBoolean(PreferenceManager.Key.IS_APP_ADS_FREE, false)) {
            if (admobInterstitialAd != null) {
                admobInterstitialAd.setFullScreenContentCallback(new FullScreenContentCallback() {
                    @Override
                    public void onAdDismissedFullScreenContent() {
                        super.onAdDismissedFullScreenContent();
                        Log.d(TAG, "onAdDismissedFullScreenContent: ");
                        admobInterstitialAd = null;
                        loadInterstitialAd(activity);
                    }

                    @Override
                    public void onAdFailedToShowFullScreenContent(AdError adError) {
                        super.onAdFailedToShowFullScreenContent(adError);
                        Log.d(TAG, "onAdFailedToShowFullScreenContent: ");
                        showFacebookInterstitialAd(activity);
                    }
                });
                admobInterstitialAd.show(activity);
            } else {
                showFacebookInterstitialAd(activity);
            }
        }
    }

    private void loadFacebookInterstitialAd(Activity activity) {
        fbInterstitialAd = new com.facebook.ads.InterstitialAd(activity, Constants.FACEBOOK_INTERSTITIAL);
        InterstitialAdListener interstitialAdListener = new InterstitialAdListener() {
            @Override
            public void onInterstitialDisplayed(Ad ad) {
                Log.e(TAG, "Interstitial ad displayed.");
            }

            @Override
            public void onInterstitialDismissed(Ad ad) {
                Log.e(TAG, "Interstitial ad dismissed.");
            }


            @Override
            public void onError(Ad ad, com.facebook.ads.AdError adError) {
                Log.e(TAG, "Interstitial ad failed to load: " + adError.getErrorMessage());
            }

            @Override
            public void onAdLoaded(Ad ad) {
                Log.d(TAG, "Interstitial ad is loaded and ready to be displayed!");
            }

            @Override
            public void onAdClicked(Ad ad) {
                Log.d(TAG, "Interstitial ad clicked!");
            }

            @Override
            public void onLoggingImpression(Ad ad) {
                Log.d(TAG, "Interstitial ad impression logged!");
            }
        };

        fbInterstitialAd.loadAd(
                fbInterstitialAd.buildLoadAdConfig()
                        .withAdListener(interstitialAdListener)
                        .build());
    }

    private void showFacebookInterstitialAd(Activity activity) {
        if (fbInterstitialAd == null || !fbInterstitialAd.isAdLoaded()) {
            return;
        }
        if (fbInterstitialAd.isAdInvalidated()) {
            return;
        }
        fbInterstitialAd.show();
    }

    public static boolean isNetWorkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetWorkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetWorkInfo != null && activeNetWorkInfo.isConnected();
    }

    private AdSize getAdSize(Activity activity) {
        // Step 2 - Determine the screen width (less decorations) to use for the ad width.
        Display display = activity.getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);
        float widthPixels = outMetrics.widthPixels;
        float density = outMetrics.density;
        int adWidth = (int) (widthPixels / density);
        // Step 3 - Get adaptive ad size and return for setting on the ad view.
        return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(activity, adWidth);
    }

    public void loadNativeAd(Activity activity, FrameLayout frameLayout, NativeAdType nativeAdViewType) {
        Log.e("activityName", activity.getClass().getSimpleName() + " frames " + frameLayout);
        if (frameLayout != null){
            if (!preferenceManager.getBoolean(PreferenceManager.Key.IS_APP_ADS_FREE, false)) {
                AdLoader.Builder builder = new AdLoader.Builder(activity, Constants.ADMOD_NATIVE);
                builder.forNativeAd(nativeAd -> {
                    NativeAdView nativeAdView = null;
                    if (nativeAdViewType == NativeAdType.REGULAR_TYPE)
                        nativeAdView = (NativeAdView) activity.getLayoutInflater().inflate(R.layout.admob_native_regular_layout, null);
                    if (nativeAdViewType == NativeAdType.MEDIUM_TYPE)
                        nativeAdView = (NativeAdView) activity.getLayoutInflater().inflate(R.layout.admob_native_medium_layout, null);
                    if (nativeAdViewType == NativeAdType.SMALL_TYPE)
                        nativeAdView = (NativeAdView) activity.getLayoutInflater().inflate(R.layout.admob_native_small_layout, null);
                    if (nativeAdViewType == NativeAdType.NOMEDIA_MEDIUM)
                        nativeAdView = (NativeAdView) activity.getLayoutInflater().inflate(R.layout.admob_native_nomedi_medium_layout, null);

                    if (nativeAdView != null)
                        populateUnifiedNativeAdView(nativeAd, nativeAdView);

                    frameLayout.removeAllViews();
                    frameLayout.addView(nativeAdView);
                    Log.d(TAG, "onNativeAdLoaded: ");
                });

                VideoOptions videoOptions =
                        new VideoOptions.Builder().setStartMuted(true).build();
                NativeAdOptions adOptions =
                        new NativeAdOptions.Builder().setVideoOptions(videoOptions).build();
                builder.withNativeAdOptions(adOptions);
                AdLoader adLoader = builder.withAdListener(
                        new AdListener() {
                            @Override
                            public void onAdFailedToLoad(LoadAdError loadAdError) {
                                Log.d(TAG, "onAdFailedToLoad: " + loadAdError);
                            }
                        })
                        .build();

                adLoader.loadAd(prepareAdRequest());
            }

    }
    }

    private void populateUnifiedNativeAdView(NativeAd nativeAd, NativeAdView adView) {
        // Set the media view.
        adView.setMediaView(adView.findViewById(R.id.ad_media));

        // Set other ad assets.
        adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
        adView.setBodyView(adView.findViewById(R.id.ad_body));
        adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));
        adView.setIconView(adView.findViewById(R.id.ad_app_icon));
        adView.setStarRatingView(adView.findViewById(R.id.ad_stars));
        adView.setStoreView(adView.findViewById(R.id.ad_store));

        // The headline and mediaContent are guaranteed to be in every UnifiedNativeAd.
        ((TextView) adView.getHeadlineView()).setText(nativeAd.getHeadline());
        adView.getMediaView().setMediaContent(nativeAd.getMediaContent());

        // These assets aren't guaranteed to be in every UnifiedNativeAd, so it's important to
        // check before trying to display them.
        if (nativeAd.getBody() == null) {
            adView.getBodyView().setVisibility(View.INVISIBLE);
        } else {
            Log.d(TAG, "populateUnifiedNativeAdView: " + nativeAd.getBody());
            adView.getBodyView().setVisibility(View.VISIBLE);
            ((TextView) adView.getBodyView()).setText(nativeAd.getBody());

        }

        if (nativeAd.getCallToAction() == null) {
            adView.getCallToActionView().setVisibility(View.INVISIBLE);
        } else {
            adView.getCallToActionView().setVisibility(View.VISIBLE);
            ((Button) adView.getCallToActionView()).setText(nativeAd.getCallToAction());
        }

        if (nativeAd.getIcon() == null) {
            adView.getIconView().setVisibility(View.GONE);
        } else {
            ((ImageView) adView.getIconView()).setImageDrawable(
                    nativeAd.getIcon().getDrawable());
            adView.getIconView().setVisibility(View.VISIBLE);
        }


        if (nativeAd.getStore() == null) {
            adView.getStoreView().setVisibility(View.INVISIBLE);
        } else {
            adView.getStoreView().setVisibility(View.VISIBLE);
            ((TextView) adView.getStoreView()).setText(nativeAd.getStore());
        }

        if (nativeAd.getStarRating() == null) {
            adView.getStarRatingView().setVisibility(View.INVISIBLE);
        } else {
            ((RatingBar) adView.getStarRatingView())
                    .setRating(nativeAd.getStarRating().floatValue());
            adView.getStarRatingView().setVisibility(View.VISIBLE);
        }

        // This method tells the Google Mobile Ads SDK that you have finished populating your
        // native ad view with this native ad.
        adView.setNativeAd(nativeAd);
        // have a video asset.
        VideoController vc = nativeAd.getMediaContent().getVideoController();

        // Updates the UI to say whether or not this ad has a video asset.
        if (vc.hasVideoContent()) {
            vc.setVideoLifecycleCallbacks(new VideoController.VideoLifecycleCallbacks() {
                @Override
                public void onVideoEnd() {
                    super.onVideoEnd();
                }
            });
        }
    }

}
